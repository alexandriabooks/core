from __future__ import annotations
from abc import abstractmethod
import typing
import asyncio
import io
import logging
import os
from operator import attrgetter
import datetime
import json
import re

from .. import config
from .. import model
from .interfaces import (
    BookMetadataService,
    BookIndexService,
    CacheService,
    EmptySearchResults, MetadataType,
    SearchResults
)
from .registry import ServiceRegistry

logger = logging.getLogger(__name__)

TERMS_RE_EXPR = re.compile(r'(([^" ]*)|"([^"]*)")')
TERMS_SUB_EXPR = re.compile(r'\W')


def sort_results(
    results: typing.List[typing.Any],
    sort_str: typing.Optional[typing.List[str]]
):
    if not results or not sort_str:
        return results

    first_result = results[0]

    sorts = [
        sort.split(":")
        for sort in sort_str
        if sort
    ]

    for sort_spec in sorts[::-1]:
        field_name = sort_spec[0]
        if not hasattr(first_result, field_name):
            continue

        reverse = False
        if len(sort_spec) > 1:
            if sort_spec[1].upper() == "DESC":
                reverse = True
                mods = [sort_spec[2:]]
            elif sort_spec[1].upper() == "ASC":
                mods = [sort_spec[2:]]
            else:
                mods = [sort_spec[1:]]
        else:
            mods = []

        if mods:
            logger.debug("Ignoring unrecognized mods: %s", str(mods))

        results.sort(key=attrgetter(field_name), reverse=reverse)
    return results


def create_cursor(**kwargs) -> str:
    return json.dumps(kwargs)


def parse_cursor(cursor: str) -> typing.Dict[str, typing.Any]:
    if not cursor:
        cursor = "{}"
    try:
        return json.loads(cursor)
    except Exception:
        return {}


def parse_terms(terms_str: str) -> typing.List[str]:
    return [
        (match[1] or match[2]).lower()
        for match in TERMS_RE_EXPR.findall(terms_str)
        if match[1] or match[2]
    ]


class InMemoryIndex(typing.Generic[MetadataType]):
    @abstractmethod
    def index(self, item: MetadataType):
        pass

    @abstractmethod
    def deindex(self, item: MetadataType):
        pass

    @abstractmethod
    def search(self, terms: typing.List[str]) -> typing.List[MetadataType]:
        pass


class TextIndex:
    def __init__(self) -> None:
        self._inverted_index: typing.Dict[
            str,
            typing.Dict[typing.Any, typing.List[int]]
        ] = {}
        self._reference_index: typing.Dict[
            typing.Any, typing.Set[str]
        ] = {}

    @staticmethod
    def split_terms(
        source: typing.Optional[str],
        terms: typing.Dict[str, typing.List[int]]
    ):
        if source:
            term_list = [
                term
                for term in TERMS_SUB_EXPR.sub(" ", source).lower().split(" ")
                if term
            ]
            for term, index in zip(term_list, range(len(term_list))):
                positions = terms.get(term)
                if not positions:
                    positions = []
                    terms[term] = positions

                positions.append(index)

    def index_terms(
        self,
        reference: typing.Any,
        terms: typing.Dict[str, typing.List[int]]
    ):
        for term, positions in terms.items():
            references = self._inverted_index.get(term)
            if references is None:
                references = {}
                self._inverted_index[term] = references
            references[reference] = positions
        self._reference_index[reference] = set(terms.keys())

    def remove_reference(self, reference: typing.Any):
        terms = self._reference_index.pop(reference, None)
        if terms:
            for term in terms:
                references = self._inverted_index.get(term)
                if references is not None:
                    references.pop(reference, None)

    def find_references(self, terms: typing.List[str]) -> typing.Set[typing.Any]:
        if not terms:
            return set()

        free_terms = []
        phrase_terms = []

        for term in terms:
            term = TERMS_SUB_EXPR.sub(" ", term).strip()
            if " " in term:
                split_phrase = term.split(" ")
                if split_phrase not in phrase_terms:
                    phrase_terms.append(split_phrase)
            else:
                if term not in free_terms:
                    free_terms.append(term)

        references = None
        if free_terms:
            initial = free_terms[0]
            references = set(self._inverted_index.get(initial, {}).keys())
            if references is None:
                return set()

            for term in free_terms[1:]:
                term_refs = self._inverted_index.get(term, {}).keys()
                references = references.intersection(term_refs)

        if phrase_terms:
            for phrase in phrase_terms:
                positions = None
                if references is None:
                    initial = phrase[0]
                    positions = self._inverted_index.get(initial, {})
                    references = set(positions.keys())
                    phrase = phrase[1:]

                for term in phrase:
                    next_positions = {
                        reference: ref_positions
                        for reference, ref_positions in self._inverted_index.get(
                            term,
                            {}
                        ).items()
                        if reference in references
                    }

                    if positions is None:
                        positions = next_positions
                    else:
                        new_positions: typing.Dict[typing.Any, typing.List[int]] = {}
                        for reference, ref_positions in next_positions.items():
                            last_positions = positions.get(reference, [])
                            for pos in last_positions:
                                if pos + 1 in ref_positions:
                                    new_positions.setdefault(
                                        reference,
                                        []
                                    ).append(pos + 1)
                        positions = new_positions
                if positions:
                    references = set(positions.keys())
                else:
                    return set()

        if not references:
            return set()
        return references


class BookMetadataIndex(InMemoryIndex[model.BookMetadata], TextIndex):
    def index(self, metadata: model.BookMetadata):
        terms: typing.Dict[str, typing.List[int]] = {}
        TextIndex.split_terms(metadata.title, terms)
        TextIndex.split_terms(metadata.subtitle, terms)
        TextIndex.split_terms(metadata.description, terms)

        for tag in metadata.tags:
            TextIndex.split_terms(tag.key, terms)
            TextIndex.split_terms(str(tag.value), terms)

        for genre in metadata.genres:
            TextIndex.split_terms(genre, terms)

        self.index_terms(metadata, terms)

    def deindex(self, item: model.BookMetadata):
        self.remove_reference(item)

    def search(self, terms: typing.List[str]) -> typing.List[model.BookMetadata]:
        result_set: typing.Set[model.BookMetadata] = self.find_references(terms)
        return list(result_set)


class PersonMetadataIndex(InMemoryIndex[model.PersonMetadata], TextIndex):
    def index(self, metadata: model.PersonMetadata):
        terms: typing.Dict[str, typing.List[int]] = {}
        TextIndex.split_terms(metadata.first_name, terms)
        TextIndex.split_terms(metadata.middle_names, terms)
        TextIndex.split_terms(metadata.last_name, terms)
        TextIndex.split_terms(metadata.bio, terms)

        for tag in metadata.tags:
            TextIndex.split_terms(tag.key, terms)
            TextIndex.split_terms(str(tag.value), terms)

        self.index_terms(metadata, terms)

    def deindex(self, item: model.PersonMetadata):
        self.remove_reference(item)

    def search(self, terms: typing.List[str]) -> typing.List[model.PersonMetadata]:
        result_set: typing.Set[model.PersonMetadata] = self.find_references(terms)
        return list(result_set)


class SeriesMetadataIndex(InMemoryIndex[model.SeriesMetadata], TextIndex):
    def index(self, metadata: model.SeriesMetadata):
        terms: typing.Dict[str, typing.List[int]] = {}
        TextIndex.split_terms(metadata.title, terms)
        TextIndex.split_terms(metadata.description, terms)

        for tag in metadata.tags:
            TextIndex.split_terms(tag.key, terms)
            TextIndex.split_terms(str(tag.value), terms)

        self.index_terms(metadata, terms)

    def deindex(self, item: model.SeriesMetadata):
        self.remove_reference(item)

    def search(self, terms: typing.List[str]) -> typing.List[model.SeriesMetadata]:
        result_set: typing.Set[model.SeriesMetadata] = self.find_references(terms)
        return list(result_set)


class InMemoryBaseSearchResults(SearchResults, typing.Generic[MetadataType]):
    def __init__(
        self,
        service: InMemoryBookMetadataService,
        offset: int
    ) -> None:
        super().__init__()

        self.service = service
        self._offset = offset
        self.result_set: typing.Optional[typing.List[MetadataType]] = None

    @abstractmethod
    async def _generate_result_set(self) -> typing.List[MetadataType]:
        """Populates the result set"""

    async def results(self) -> typing.AsyncGenerator[MetadataType, None]:
        """Get results iterator."""
        if self.result_set is None:
            async with self.service._lock:
                result_set = await self._generate_result_set()
            self.result_set = result_set
            self._offset = max(min(self._offset, len(result_set)), 0)
        else:
            result_set = self.result_set

        for item in result_set[self._offset:]:
            self._offset += 1
            yield item

    @property
    def has_next(self) -> bool:
        """True if there are more results available."""
        return self.result_set is None or len(self.result_set) > self._offset

    @property
    def at_beginning(self) -> bool:
        """True if the cursor is at the beginning of the result set."""
        return self._offset == 0

    @property
    def offset(self) -> int:
        """The current offset."""
        return self._offset

    @property
    def remaining(self) -> int:
        """Estimate of how many are remaining."""
        if self.result_set is None:
            return -1
        return len(self.result_set) - self._offset

    async def skip(self, count: int) -> SearchResults[MetadataType]:
        """Fast forwards or rewinds the cursor."""
        if self.result_set is not None:
            self._offset = max(min(self._offset + count, len(self.result_set)), 0)
        else:
            self._offset = max(self._offset + count, 0)
        return self


class InMemoryBookSearchResults(InMemoryBaseSearchResults[model.BookMetadata]):
    def __init__(
        self,
        service: InMemoryBookMetadataService,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        source_types: typing.Optional[typing.List[model.SourceType]] = None,
        series_ids: typing.Optional[typing.List[str]] = None,
        person_ids: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False,
        offset: int = 0
    ) -> None:
        super().__init__(
            service,
            offset
        )
        self.terms = terms
        self.sort = sort
        self.source_types = source_types
        self.series_ids = series_ids
        self.person_ids = person_ids
        self.include_sourceless = include_sourceless

    @property
    def cursor(self) -> str:
        """Returns a string representation of the cursor."""
        source_types_str: typing.Optional[typing.List[str]] = None
        if self.source_types:
            source_types_str = [
                entry.name
                for entry in self.source_types
            ]

        return create_cursor(
            terms=self.terms,
            sort=self.sort,
            offset=self._offset,
            source_types=source_types_str,
            series_ids=self.series_ids,
            person_ids=self.person_ids,
            include_sourceless=self.include_sourceless
        )

    async def _generate_result_set(self) -> typing.List[model.BookMetadata]:
        results: typing.List[model.BookMetadata]
        if self.terms:
            results = self.service.book_index.search(parse_terms(self.terms))
        else:
            results = list(self.service.books.values())

        if self.series_ids or self.person_ids or not self.include_sourceless:
            filtered_results = []

            for book_metadata in results:
                if not self.include_sourceless and not book_metadata.source_metadata:
                    continue

                if self.source_types:
                    for source_metadata in book_metadata.source_metadata.values():
                        if source_metadata.source_type in self.source_types:
                            break
                    else:
                        continue

                if self.series_ids:
                    for series_entry in book_metadata.series:
                        if series_entry.series_id in self.series_ids:
                            break
                    else:
                        continue

                if self.person_ids:
                    for people_id in book_metadata.people:
                        if people_id in self.person_ids:
                            break
                    else:
                        continue

                filtered_results.append(book_metadata)

            results = filtered_results
        sort_results(results, self.sort)
        return results

    @staticmethod
    def from_cursor(
        service: InMemoryBookMetadataService,
        cursor: str
    ) -> SearchResults[model.BookMetadata]:
        parsed = parse_cursor(cursor)
        if not parsed:
            return EmptySearchResults()
        return InMemoryBookSearchResults(service, **parsed)


class InMemoryPeopleSearchResults(InMemoryBaseSearchResults[model.PersonMetadata]):
    def __init__(
        self,
        service: InMemoryBookMetadataService,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False,
        offset: int = 0
    ) -> None:
        super().__init__(
            service,
            offset
        )
        self.terms = terms
        self.sort = sort
        self.include_sourceless = include_sourceless

    @property
    def cursor(self) -> str:
        """Returns a string representation of the cursor."""
        return create_cursor(
            terms=self.terms,
            sort=self.sort,
            offset=self._offset,
            include_sourceless=self.include_sourceless
        )

    async def _generate_result_set(self) -> typing.List[model.PersonMetadata]:
        results: typing.List[model.PersonMetadata]
        if self.terms:
            results = self.service.person_index.search(parse_terms(self.terms))
        else:
            results = list(self.service.persons.values())

        if not self.include_sourceless:
            filtered_results = []

            for person_metadata in results:
                if not self.include_sourceless and not person_metadata.source_metadata:
                    continue

                filtered_results.append(person_metadata)

            results = filtered_results

        sort_results(results, self.sort)
        return results

    @staticmethod
    def from_cursor(
        service: InMemoryBookMetadataService,
        cursor: str
    ) -> SearchResults[model.PersonMetadata]:
        parsed = parse_cursor(cursor)
        if not parsed:
            return EmptySearchResults()
        return InMemoryPeopleSearchResults(service, **parsed)


class InMemorySeriesSearchResults(InMemoryBaseSearchResults[model.SeriesMetadata]):
    def __init__(
        self,
        service: InMemoryBookMetadataService,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False,
        offset: int = 0
    ) -> None:
        super().__init__(
            service,
            offset
        )
        self.terms = terms
        self.sort = sort
        self.include_sourceless = include_sourceless

    @property
    def cursor(self) -> str:
        """Returns a string representation of the cursor."""
        return create_cursor(
            terms=self.terms,
            sort=self.sort,
            offset=self._offset,
            include_sourceless=self.include_sourceless
        )

    async def _generate_result_set(self) -> typing.List[model.SeriesMetadata]:
        results: typing.List[model.SeriesMetadata]
        if self.terms:
            results = self.service.series_index.search(parse_terms(self.terms))
        else:
            results = list(self.service.series.values())

        if not self.include_sourceless:
            filtered_results = []

            for person_metadata in results:
                if not self.include_sourceless and not person_metadata.source_metadata:
                    continue

                filtered_results.append(person_metadata)

            results = filtered_results

        sort_results(results, self.sort)
        return results

    @staticmethod
    def from_cursor(
        service: InMemoryBookMetadataService,
        cursor: str
    ) -> SearchResults[model.SeriesMetadata]:
        parsed = parse_cursor(cursor)
        if not parsed:
            return EmptySearchResults()
        return InMemorySeriesSearchResults(service, **parsed)


class InMemoryBook(model.Book):
    def __init__(
        self,
        metadata_service: InMemoryBookMetadataService,
        book_id: str,
        metadata: model.BookMetadata
    ):
        super().__init__()
        self._metadata_service: InMemoryBookMetadataService = metadata_service
        self._book_id: str = book_id
        self._metadata: model.BookMetadata = metadata
        self._sources: typing.Optional[typing.List[model.BookSource]] = None

    @property
    def book_id(self) -> str:
        """Returns the book ID for this source."""
        return self._book_id

    def get_metadata(self) -> model.BookMetadata:
        """Returns metadata for this book."""
        return self._metadata

    async def get_sources(self) -> typing.List[model.BookSource]:
        """Returns a BookSource for each source of this book."""
        if self._sources is None:
            sources: typing.List[model.BookSource] = []
            for source_metadata in self.get_metadata().source_metadata.values():
                source = await self._metadata_service._service_registry.get_book_source(
                    source_metadata
                )
                if source is not None:
                    sources.append(source)
            self._sources = sources
        return typing.cast(typing.List[model.BookSource], self._sources)


class InMemoryBookMetadataService(BookMetadataService, BookIndexService):
    """Implements in-memory metadata service without persistence."""

    def __init__(self, service_registry: ServiceRegistry):
        self._service_registry: ServiceRegistry = service_registry

        self.books: typing.Dict[str, model.BookMetadata] = {}
        self.book_source_mapping: typing.Dict[str, str] = {}
        self.book_index = BookMetadataIndex()

        self.series: typing.Dict[str, model.SeriesMetadata] = {}
        self.series_source_mapping: typing.Dict[str, str] = {}
        self.series_index = SeriesMetadataIndex()

        self.persons: typing.Dict[str, model.PersonMetadata] = {}
        self.person_source_mapping: typing.Dict[str, str] = {}
        self.person_index = PersonMetadataIndex()

        self._lock = asyncio.Lock()
        self._snapshot_lock = asyncio.Lock()
        self.snapshot_directory = config.get("inmemory.snapshot.directory")
        self.max_snapshots = int(config.get("inmemory.snapshot.max") or "5")
        self._snapshot_handle = None
        if self.snapshot_directory:
            if self.max_snapshots:
                logger.info(
                    "Snapshotting configured, will only keep last %s snapshots",
                    self.max_snapshots
                )
            else:
                logger.warning(
                    "Will keep all snapshots, please ensure this is what you want."
                )

            for snapshot_timestamp in self.get_snapshot_list():
                try:
                    self.load_snapshot(snapshot_timestamp)
                    logger.info("Loaded snapshot %s", snapshot_timestamp)
                    break
                except Exception as e:
                    logger.warning(
                        "Could not load snapshot %i",
                        snapshot_timestamp,
                        exc_info=e
                    )

    def reload(self):
        self.snapshot_directory = config.get("inmemory.snapshot_directory")

    async def _do_snapshot(self):
        await asyncio.sleep(15.0)
        async with self._snapshot_lock:
            snapshot_directory = self.snapshot_directory
            if not snapshot_directory:
                return

            timestamp = int(datetime.datetime.now().timestamp())

            if not os.path.isdir(snapshot_directory):
                os.makedirs(snapshot_directory)

            logger.debug("Building snapshot data...")
            snapshot_data = {
                "books": {
                    key: value.as_dict()
                    for key, value in self.books.items()
                },
                "book_source_mapping": self.book_source_mapping,
                "series": {
                    key: value.as_dict()
                    for key, value in self.series.items()
                },
                "series_source_mapping": self.series_source_mapping,
                "persons": {
                    key: value.as_dict()
                    for key, value in self.persons.items()
                },
                "person_source_mapping": self.person_source_mapping
            }

            def write_snapshot():
                snapshot_name = os.path.join(
                    snapshot_directory,
                    f"snapshot.{timestamp}.json"
                )
                logger.info("Creating snapshot into %s", snapshot_name)
                success = False
                try:
                    with open(snapshot_name, "w", encoding="utf-8") as handle:
                        json.dump(snapshot_data, handle, indent=2)
                    logger.info("Snapshot created!")
                    if self.max_snapshots:
                        existing_snapshots = self.get_snapshot_list()
                        if len(existing_snapshots) > self.max_snapshots:
                            for idx in range(self.max_snapshots, len(existing_snapshots)):
                                self.delete_snapshot(existing_snapshots[idx])
                    success = True
                finally:
                    if not success:
                        logger.info("Removing failed snapshot %s", snapshot_name)
                        os.unlink(snapshot_name)

            await asyncio.get_running_loop().run_in_executor(None, write_snapshot)

    def get_snapshot_list(self) -> typing.List[int]:
        snapshots = []
        if self.snapshot_directory and os.path.isdir(self.snapshot_directory):
            for filename in os.listdir(self.snapshot_directory):
                if (
                    filename.startswith("snapshot.") and
                    filename.endswith(".json")
                ):
                    timestamp_str = filename[9:-5]
                    if timestamp_str.isnumeric():
                        timestamp = int(timestamp_str)
                        snapshots.append(
                            timestamp
                        )
        snapshots.sort(reverse=True)
        return snapshots

    def delete_snapshot(self, timestamp: int):
        if not self.snapshot_directory:
            return
        snapshot_name = os.path.join(
            self.snapshot_directory,
            f"snapshot.{timestamp}.json"
        )
        logger.debug("Deleting snapshot %s", snapshot_name)
        os.unlink(snapshot_name)

    def load_snapshot(self, timestamp: int):
        if not self.snapshot_directory:
            return
        snapshot_name = os.path.join(
            self.snapshot_directory,
            f"snapshot.{timestamp}.json"
        )
        logger.debug("Loading snapshot %s", snapshot_name)
        with open(snapshot_name, "r", encoding="utf-8") as handle:
            snapshot_data = json.load(handle)
        books = {
            book_id: model.BookMetadata.from_dict(book)
            for book_id, book in snapshot_data.get("books", {}).items()
        }
        book_source_mapping = snapshot_data.get("book_source_mapping", {})
        series = {
            series_id: model.SeriesMetadata.from_dict(series)
            for series_id, series in snapshot_data.get("series", {}).items()
        }
        series_source_mapping = snapshot_data.get("series_source_mapping", {})
        persons = {
            person_id: model.PersonMetadata.from_dict(person)
            for person_id, person in snapshot_data.get("persons", {}).items()
        }
        person_source_mapping = snapshot_data.get("person_source_mapping", {})

        self.books = books
        self.book_source_mapping = book_source_mapping
        for book_entry in books.values():
            self.book_index.index(book_entry)

        self.series = series
        self.series_source_mapping = series_source_mapping
        for series_entry in series.values():
            self.series_index.index(series_entry)

        self.persons = persons
        self.person_source_mapping = person_source_mapping
        for person_entry in persons.values():
            self.person_index.index(person_entry)
        logger.debug("Snapshot loaded")

    async def schedule_snapshot(self):
        if not self.snapshot_directory:
            return

        if self._snapshot_handle:
            self._snapshot_handle.cancel()
        self._snapshot_handle = asyncio.create_task(self._do_snapshot())

    def __str__(self):
        return "In Memory Metadata Service"

    async def search_books(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        source_types: typing.Optional[typing.List[model.SourceType]] = None,
        series_ids: typing.Optional[typing.List[str]] = None,
        person_ids: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False
    ) -> SearchResults[model.BookMetadata]:

        return InMemoryBookSearchResults(
            self,
            terms,
            sort,
            source_types,
            series_ids,
            person_ids,
            include_sourceless
        )

    async def continue_search_books(
        self,
        cursor: str,
    ) -> SearchResults[model.BookMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """
        return InMemoryBookSearchResults.from_cursor(self, cursor)

    async def search_people(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False
    ) -> SearchResults[model.PersonMetadata]:

        return InMemoryPeopleSearchResults(
            self,
            terms,
            sort,
            include_sourceless
        )

    async def continue_search_people(
        self,
        cursor: str,
    ) -> SearchResults[model.PersonMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """
        return InMemoryPeopleSearchResults.from_cursor(self, cursor)

    async def search_series(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False
    ) -> SearchResults[model.SeriesMetadata]:

        return InMemorySeriesSearchResults(
            self,
            terms,
            sort,
            include_sourceless
        )

    async def continue_search_series(
        self,
        cursor: str,
    ) -> SearchResults[model.SeriesMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """
        return InMemorySeriesSearchResults.from_cursor(self, cursor)

    async def set_book_user_metadata(self, book_id: str, **kwargs):
        async with self._lock:
            if book_id not in self.books:
                book = model.BookMetadata(book_id=book_id)
                self.books[book_id] = book
            else:
                book = self.books[book_id]

            book.user_metadata.set_metadata(**kwargs)

            self.book_index.deindex(book)
            self.book_index.index(book)

            await self.schedule_snapshot()

    async def add_book_source(
        self, source_metadata: model.SourceProvidedBookMetadata, book_id: str = ""
    ) -> str:
        async with self._lock:
            if not book_id:
                book_id = source_metadata.source_key

            book_id = self.book_source_mapping.setdefault(
                source_metadata.source_key, book_id
            )
            if book_id not in self.books:
                book = model.BookMetadata(book_id=book_id)
                self.books[book_id] = book
            else:
                book = self.books[book_id]

            book.source_metadata[source_metadata.source_key] = source_metadata

            self.book_index.deindex(book)
            self.book_index.index(book)

            await self.schedule_snapshot()
            return book_id

    async def remove_book_source(self, source_key: str):
        async with self._lock:
            if source_key in self.book_source_mapping:
                book_id = self.book_source_mapping.pop(source_key, "")
                if not book_id:
                    return
                book = self.books.get(book_id)
                if not book:
                    return

                if source_key in book.source_metadata:
                    del book.source_metadata[source_key]

                self.book_index.deindex(book)
                if (
                    not book.source_metadata and
                    not book.scraper_metadata and
                    not book.user_metadata
                ):
                    del self.books[book_id]
                else:
                    self.book_index.index(book)

            await self.schedule_snapshot()

    async def add_book_scraper_source(
        self, book_id: str, scraper_metadata: model.ScraperProvidedBookMetadata
    ):
        async with self._lock:
            if book_id not in self.books:
                book = model.BookMetadata(book_id=book_id)
                self.books[book_id] = book
            else:
                book = self.books[book_id]

            book.scraper_metadata[scraper_metadata.scraper_key] = scraper_metadata

            self.book_index.deindex(book)
            self.book_index.index(book)

            await self.schedule_snapshot()

    async def remove_book_scraper_source(self, book_id: str, scraper_key: str):
        async with self._lock:
            book = self.books.get(book_id)
            if not book:
                return

            if scraper_key in book.scraper_metadata:
                del book.scraper_metadata[scraper_key]

            self.book_index.deindex(book)
            if (
                not book.source_metadata and
                not book.scraper_metadata and
                not book.user_metadata
            ):
                del self.books[book_id]
            else:
                self.book_index.index(book)

            await self.schedule_snapshot()

    async def get_book_by_id(self, book_id):
        async with self._lock:
            book_metadata = self.books.get(book_id)
            if book_metadata:
                return InMemoryBook(self, book_id, book_metadata)
        return None

    async def get_book_source_metadata_by_key(
        self,
        source_key: str
    ) -> typing.Optional[model.SourceProvidedBookMetadata]:
        """
        Returns source metadata for a given source key.
        :param book_id: The source key.
        :return: The source metadata for the given source key.
        """
        async with self._lock:
            book_id = self.book_source_mapping.get(source_key)
            if not book_id:
                return None

            book_metadata = self.books.get(book_id, None)
            if not book_metadata:
                return None
        return book_metadata.source_metadata.get(source_key)

    async def set_series_user_metadata(self, series_id: str, **kwargs):
        async with self._lock:
            if series_id not in self.series:
                series = model.SeriesMetadata(series_id=series_id)
                self.series[series_id] = series
            else:
                series = self.series[series_id]

            series.user_metadata.set_metadata(**kwargs)

            self.series_index.deindex(series)
            self.series_index.index(series)

            await self.schedule_snapshot()

    async def add_series_source(
        self, source_metadata: model.SourceProvidedSeriesMetadata, series_id: str = ""
    ) -> str:
        async with self._lock:
            if not series_id:
                series_id = source_metadata.source_key

            series_id = self.series_source_mapping.setdefault(
                source_metadata.source_key, series_id
            )
            if series_id not in self.series:
                series = model.SeriesMetadata(series_id=series_id)
                self.series[series_id] = series
            else:
                series = self.series[series_id]

            series.source_metadata[source_metadata.source_key] = source_metadata

            self.series_index.deindex(series)
            self.series_index.index(series)

            await self.schedule_snapshot()
            return series_id

    async def remove_series_source(self, source_key: str):
        async with self._lock:
            if source_key in self.series_source_mapping:
                series_id = self.series_source_mapping.pop(source_key, "")
                if not series_id:
                    return
                series = self.series.get(series_id)
                if not series:
                    return

                if source_key in series.source_metadata:
                    del series.source_metadata[source_key]

                self.series_index.deindex(series)
                if (
                    not series.source_metadata and
                    not series.scraper_metadata and
                    not series.user_metadata
                ):
                    del self.series[series_id]
                else:
                    self.series_index.index(series)
            await self.schedule_snapshot()

    async def add_series_scraper_source(
        self, series_id: str, scraper_metadata: model.ScraperProvidedSeriesMetadata
    ):
        async with self._lock:
            if series_id not in self.series:
                series = model.SeriesMetadata(series_id=series_id)
                self.series[series_id] = series
            else:
                series = self.series[series_id]

            series.scraper_metadata[scraper_metadata.scraper_key] = scraper_metadata

            self.series_index.deindex(series)
            self.series_index.index(series)

            await self.schedule_snapshot()

    async def remove_series_scraper_source(self, series_id: str, scraper_key: str):
        async with self._lock:
            series = self.series.get(series_id)
            if not series:
                return

            if scraper_key in series.scraper_metadata:
                del series.scraper_metadata[scraper_key]

            self.series_index.deindex(series)
            if (
                not series.source_metadata and
                not series.scraper_metadata and
                not series.user_metadata
            ):
                del self.series[series_id]
            else:
                self.series_index.index(series)
            await self.schedule_snapshot()

    async def get_series_metadata_by_id(
        self,
        series_id: str
    ) -> typing.Optional[model.SeriesMetadata]:
        """
        Returns series metadata for a given series ID.
        :param series_id: The series ID.
        :return: The metadata for the given series ID.
        """
        async with self._lock:
            return self.series.get(series_id)

    async def set_person_user_metadata(self, person_id: str, **kwargs):
        async with self._lock:
            if person_id not in self.persons:
                person = model.PersonMetadata(person_id=person_id)
                self.persons[person_id] = person
            else:
                person = self.persons[person_id]

            person.user_metadata.set_metadata(**kwargs)

            self.person_index.deindex(person)
            self.person_index.index(person)

            await self.schedule_snapshot()

    async def add_person_source(
        self, source_metadata: model.SourceProvidedPersonMetadata, person_id: str = ""
    ) -> str:
        async with self._lock:
            if not person_id:
                person_id = source_metadata.source_key

            person_id = self.person_source_mapping.setdefault(
                source_metadata.source_key, person_id
            )
            if person_id not in self.persons:
                person = model.PersonMetadata(person_id=person_id)
                self.persons[person_id] = person
            else:
                person = self.persons[person_id]

            person.source_metadata[source_metadata.source_key] = source_metadata

            self.person_index.deindex(person)
            self.person_index.index(person)

            await self.schedule_snapshot()
            return person_id

    async def remove_person_source(self, source_key: str):
        async with self._lock:
            if source_key in self.person_source_mapping:
                person_id = self.person_source_mapping.pop(source_key, "")
                if not person_id:
                    return
                person = self.persons.get(person_id)
                if not person:
                    return

                if source_key in person.source_metadata:
                    del person.source_metadata[source_key]

                self.person_index.deindex(person)
                if (
                    not person.source_metadata and
                    not person.scraper_metadata and
                    not person.user_metadata
                ):
                    del self.persons[person_id]
                else:
                    self.person_index.index(person)
            await self.schedule_snapshot()

    async def add_person_scraper_source(
        self, person_id: str, scraper_metadata: model.ScraperProvidedPersonMetadata
    ):
        async with self._lock:
            if person_id not in self.persons:
                person = model.PersonMetadata(person_id=person_id)
                self.persons[person_id] = person
            else:
                person = self.persons[person_id]

            person.scraper_metadata[scraper_metadata.scraper_key] = scraper_metadata

            self.person_index.deindex(person)
            self.person_index.index(person)

            await self.schedule_snapshot()

    async def remove_person_scraper_source(self, person_id: str, scraper_key: str):
        async with self._lock:
            person = self.persons.get(person_id)
            if not person:
                return

            if scraper_key in person.scraper_metadata:
                del person.scraper_metadata[scraper_key]

            self.person_index.deindex(person)
            if (
                not person.source_metadata and
                not person.scraper_metadata and
                not person.user_metadata
            ):
                del self.persons[person_id]
            else:
                self.person_index.index(person)

            await self.schedule_snapshot()

    async def get_person_metadata_by_id(
        self,
        person_id: str
    ) -> typing.Optional[model.PersonMetadata]:
        """
        Returns person metadata for a given person ID.
        :param person_id: The person ID.
        :return: The metadata for the given person ID.
        """
        async with self._lock:
            return self.persons.get(person_id)


class NoOpCache(CacheService):
    """Implements a CacheService that doesn't cache."""

    def __str__(self):
        return "No-Op Cache"

    async def get_stream(self, key, value_func, max_age=None):
        dest_stream = io.BytesIO()
        success = await value_func(dest_stream)
        if not success:
            return None
        dest_stream.seek(0)
        return dest_stream

    async def cache_expired(self, key, max_age=None):
        return True
