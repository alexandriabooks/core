import typing
from .interfaces import (
    BackgroundService,
    BookMetadataService,
    CacheService,
    BookIndexService,
    ImageTranscoderService,
    AudioTranscoderService,
    BookTranscoderService,
    BookParserService,
    BookSourceProviderService,
    ServiceFilter,
    ALL_SERVICES_FILTER
)
from .registry import ServiceRegistry

__all__ = [
    "BackgroundService",
    "BookMetadataService",
    "CacheService",
    "BookIndexService",
    "ImageTranscoderService",
    "AudioTranscoderService",
    "BookTranscoderService",
    "BookParserService",
    "BookSourceProviderService",
    "ServiceFilter",
    "ALL_SERVICES_FILTER",

    "ServiceRegistry"
]

ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {}
try:
    from .configwatcher import ConfigWatcher
    ALEXANDRIA_PLUGINS["BackgroundService"] = [ConfigWatcher]
except ImportError:
    pass
