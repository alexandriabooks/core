import logging
import typing
import asyncio
import os.path

from .. import config
from .interfaces import BackgroundService
from .registry import ServiceRegistry

import inotify.adapters
import inotify.constants

logger = logging.getLogger(__name__)


class ConfigWatcher(BackgroundService):
    def __init__(self, service_registry: ServiceRegistry):
        super().__init__()
        self._service_registry = service_registry
        self.inotify_watcher: typing.Optional[inotify.adapters.Inotify] = None

    def __str__(self):
        return "Config Watcher"

    async def initialize(self):
        """Initialize the service."""

    async def start(self):
        """Start the service."""
        logger.info("Starting config watcher...")
        self.inotify_watcher = inotify.adapters.Inotify()
        self.inotify_watcher.add_watch(
            os.path.dirname(os.path.abspath(config.get_config_file())),
            mask=(
                inotify.constants.IN_CLOSE_WRITE |
                inotify.constants.IN_CREATE |
                inotify.constants.IN_DELETE
            )
        )
        self._refresh_task = asyncio.ensure_future(self._refresh_thread())

    async def stop(self):
        """Start the service."""
        if self._refresh_task is not None:
            self._refresh_task.cancel()

        if self.inotify_watcher:
            del self.inotify_watcher.inotify
            self.inotify_watcher = None

    async def _refresh_thread(self):
        config_file = os.path.abspath(config.get_config_file())

        def check_inotify():
            return list(self.inotify_watcher.event_gen(
                yield_nones=False,
                timeout_s=10
            ))

        while True:
            inotify_events = await asyncio.get_running_loop().run_in_executor(
                None,
                check_inotify
            )
            for _, type_names, path, filename in inotify_events:
                logger.debug(f"Got inotify event: {type_names} {path} {filename}")
                if 'IN_ISDIR' in type_names:
                    continue
                full_path = os.path.join(path, filename)
                if os.path.samefile(full_path, config_file) and os.path.isfile(full_path):
                    logger.info("Detected configuration file changes, reloading...")
                    config.reload()
                    await self._service_registry.reload()
