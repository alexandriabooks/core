from __future__ import annotations
import logging
import typing
from abc import ABC, abstractmethod, abstractproperty
from .. import model

logger = logging.getLogger(__name__)


T = typing.TypeVar('T')


class ServiceFilter:
    def __init__(self):
        super().__init__()
        self.excluded_instances = []
        self.excluded_classes = []

    def exclude(self, service):
        if isinstance(service, type):
            self.excluded_classes.append(service)
        else:
            self.excluded_instances.append(service)

    def filter(
        self,
        service_list: typing.List[T]
    ) -> typing.Iterator[T]:
        excluded_class_tuple = tuple(self.excluded_classes)
        for service in service_list:
            if service in self.excluded_instances:
                continue
            if excluded_class_tuple:
                if isinstance(service, excluded_class_tuple):
                    continue
            yield service

    def filter_dict(
        self,
        service_dict: typing.Dict[str, T]
    ) -> typing.Iterator[typing.Tuple[str, T]]:
        excluded_class_tuple = tuple(self.excluded_classes)
        for service_id, service in service_dict.items():
            if service in self.excluded_instances:
                continue
            if excluded_class_tuple:
                if isinstance(service, excluded_class_tuple):
                    continue
            yield service_id, service


ALL_SERVICES_FILTER = ServiceFilter()


class BackgroundService(ABC):
    async def initialize(self):
        """Initialize the service."""

    async def start(self):
        """Start the service."""

    async def stop(self):
        """Start the service."""


MetadataType = typing.TypeVar(
    'MetadataType',
    model.BookMetadata,
    model.PersonMetadata,
    model.SeriesMetadata
)


class SearchResults(typing.Generic[MetadataType]):
    @abstractproperty
    def cursor(self) -> str:
        """Returns a string representation of the cursor."""

    @abstractmethod
    async def results(self) -> typing.AsyncGenerator[MetadataType, None]:
        """Get results iterator."""
        for _ in []:
            yield _

    @abstractproperty
    def has_next(self) -> bool:
        """True if there are more results available."""

    @abstractproperty
    def at_beginning(self) -> bool:
        """True if the cursor is at the beginning of the result set."""

    @property
    def offset(self) -> int:
        """The current offset."""
        return -1

    @property
    def remaining(self) -> int:
        """Estimate of how many are remaining."""
        return -1

    @abstractmethod
    async def skip(self, count: int) -> SearchResults[MetadataType]:
        """Fast forwards or rewinds the cursor."""
        pass


class EmptySearchResults(SearchResults[MetadataType]):
    @property
    def cursor(self) -> str:
        return ""

    async def results(self) -> typing.AsyncGenerator[MetadataType, None]:
        """Get results iterator."""
        for _ in []:
            yield _

    @property
    def has_next(self) -> bool:
        """True if there are more results available."""
        return False

    @property
    def at_beginning(self) -> bool:
        """True if the cursor is at the beginning of the result set."""
        return True

    @property
    def offset(self) -> int:
        """The current offset."""
        return 0

    @property
    def remaining(self) -> int:
        """Estimate of how many are remaining."""
        return 0

    async def skip(self, count: int) -> SearchResults[MetadataType]:
        """Fast forwards or rewinds the cursor."""
        return self


class BookIndexService(ABC):
    """Provides methods for searching for books."""

    @abstractmethod
    async def search_books(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        source_types: typing.Optional[typing.List[model.SourceType]] = None,
        series_ids: typing.Optional[typing.List[str]] = None,
        person_ids: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False
    ) -> SearchResults[model.BookMetadata]:
        """
        Searches using the given terms.
        :param terms: The search string.
        :param sort: How to sort the results.
        :param source_types: The types of books to return.
        :param series_ids: Limit the search to these series.
        :param person_ids: Limit the search to books involved with these people.
        :param include_sourceless: Include results without sources.
        :return: The search results.
        """

    @abstractmethod
    async def continue_search_books(
        self,
        cursor: str,
    ) -> SearchResults[model.BookMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """

    @abstractmethod
    async def search_people(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False
    ) -> SearchResults[model.PersonMetadata]:
        """
        Searches using the given terms.
        :param terms: The search string.
        :param sort: How to sort the results.
        :param include_sourceless: Include results without sources.
        :return: The matching people.
        """

    @abstractmethod
    async def continue_search_people(
        self,
        cursor: str,
    ) -> SearchResults[model.PersonMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """

    @abstractmethod
    async def search_series(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False
    ) -> SearchResults[model.SeriesMetadata]:
        """
        Searches using the given terms.
        :param terms: The search string.
        :param sort: How to sort the results.
        :param include_sourceless: Include results without sources.
        :return: The matching series.
        """

    @abstractmethod
    async def continue_search_series(
        self,
        cursor: str,
    ) -> SearchResults[model.SeriesMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """


class BookMetadataService(ABC):
    """Provides methods for storing book metadata"""

    @abstractmethod
    async def set_book_user_metadata(self, book_id: str, **kwargs):
        """
        Updates the user provided book metadata with the given values.
        :param book_id: The book id to add the scraper metadata to.
        :param kwargs: The book metadata to update.
        """

    @abstractmethod
    async def add_book_source(
        self, source_metadata: model.SourceProvidedBookMetadata, book_id: str = ""
    ) -> str:
        """
        Adds/updates book source metadata to the metadata service.
        :param source_metadata: The book metadata to add.
        :param book_id: A suggested book id.
        :return: The assigned book id.
        """

    @abstractmethod
    async def remove_book_source(self, source_key: str):
        """
        Removes book source metadata.
        :param source_key: The source to remove.
        """

    @abstractmethod
    async def add_book_scraper_source(
        self, book_id: str, scraper_metadata: model.ScraperProvidedBookMetadata
    ):
        """
        Adds/updates book source metadata to the metadata service.
        :param book_id: The book id to add the scraper metadata to.
        :param scraper_metadata: The book metadata to add.
        """

    @abstractmethod
    async def remove_book_scraper_source(self, book_id: str, scraper_key: str):
        """
        Removes book scraper source metadata.
        :param scraper_key: The source to remove.
        """

    @abstractmethod
    async def get_book_by_id(self, book_id: str) -> typing.Optional[model.Book]:
        """
        Returns book for a given book ID.
        :param book_id: The book ID.
        :return: The book for the given book ID.
        """

    @abstractmethod
    async def get_book_source_metadata_by_key(
        self,
        source_key: str
    ) -> typing.Optional[model.SourceProvidedBookMetadata]:
        """
        Returns book source metadata for a given source key.
        :param book_id: The source key.
        :return: The source metadata for the given source key.
        """

    @abstractmethod
    async def set_series_user_metadata(
        self,
        series_id: str,
        **kwargs
    ):
        """
        Updates the user provided metadata with the given values.
        :param series_id: The series id to add the scraper metadata to.
        :param kwargs: The metadata to update.
        """

    @abstractmethod
    async def add_series_source(
        self,
        source_metadata: model.SourceProvidedSeriesMetadata,
        series_id: str = ""
    ) -> str:
        """
        Adds/updates series source metadata to the metadata service.
        :param source_metadata: The metadata to add.
        :param series_id: A suggested series id.
        :return: The assigned series id.
        """

    @abstractmethod
    async def remove_series_source(self, source_key: str):
        """
        Removes book series metadata.
        :param source_key: The source to remove.
        """

    @abstractmethod
    async def add_series_scraper_source(
        self,
        series_id: str,
        scraper_metadata: model.ScraperProvidedSeriesMetadata
    ):
        """
        Adds/updates series source metadata to the metadata service.
        :param series_id: The series id to add the scraper metadata to.
        :param scraper_metadata: The metadata to add.
        """

    @abstractmethod
    async def remove_series_scraper_source(self, series_id: str, scraper_key: str):
        """
        Removes series scraper source metadata.
        :param scraper_key: The source to remove.
        """

    @abstractmethod
    async def get_series_metadata_by_id(
        self,
        series_id: str
    ) -> typing.Optional[model.SeriesMetadata]:
        """
        Returns series metadata for a given series ID.
        :param series_id: The series ID.
        :return: The metadata for the given series ID.
        """

    @abstractmethod
    async def set_person_user_metadata(
        self,
        person_id: str,
        **kwargs
    ):
        """
        Updates the user provided metadata with the given values.
        :param person_id: The person id to add the scraper metadata to.
        :param kwargs: The metadata to update.
        """

    @abstractmethod
    async def add_person_source(
        self,
        source_metadata: model.SourceProvidedPersonMetadata,
        person_id: str = ""
    ) -> str:
        """
        Adds/updates person source metadata to the metadata service.
        :param source_metadata: The metadata to add.
        :param person_id: A suggested person id.
        :return: The assigned person id.
        """

    @abstractmethod
    async def remove_person_source(self, source_key: str):
        """
        Removes person source metadata.
        :param source_key: The source to remove.
        """

    @abstractmethod
    async def add_person_scraper_source(
        self,
        person_id: str,
        scraper_metadata: model.ScraperProvidedPersonMetadata
    ):
        """
        Adds/updates person source metadata to the metadata service.
        :param person_id: The person id to add the scraper metadata to.
        :param scraper_metadata: The metadata to add.
        """

    @abstractmethod
    async def remove_person_scraper_source(self, person_id: str, scraper_key: str):
        """
        Removes person scraper source metadata.
        :param scraper_key: The source to remove.
        """

    @abstractmethod
    async def get_person_metadata_by_id(
        self,
        person_id: str
    ) -> typing.Optional[model.PersonMetadata]:
        """
        Returns person metadata for a given person ID.
        :param person_id: The person ID.
        :return: The metadata for the given person ID.
        """


class BookSourceProviderService(ABC):
    """Provides methods for book retrieval."""

    @abstractmethod
    async def refresh_source_provider(self):
        """Refresh the source provider."""

    @abstractmethod
    def get_prefix(self) -> str:
        """Returns the source prefix for this source."""

    @abstractmethod
    async def get_book_source(
        self,
        source_metadata: model.SourceProvidedBookMetadata
    ) -> typing.Optional[model.BookSource]:
        """
        Gets an accessor for the given book.
        :param source_metadata: The source metadata.
        :return: The book's accessor, or None if it cannot be found.
        """

    @abstractmethod
    async def get_book_source_by_key(
        self,
        source_key: str
    ) -> typing.Optional[model.BookSource]:
        """
        Gets a book source for the source key.
        :param source_key: The source key.
        :return: The book's accessor, or None if it cannot be found.
        """


class ImageTranscoderService(ABC):
    """Provides methods for transcoding images."""

    @abstractmethod
    async def get_image_transcode_targets(
        self, src: model.ImageType
    ) -> typing.List[model.ImageType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """

    @abstractmethod
    async def transcode_image(
        self,
        src: typing.IO[bytes],
        desired_type: model.ImageType,
        desired_size: typing.Tuple[int, int] = None,
        max_size: typing.Tuple[int, int] = None,
        greyscale: bool = None,
        out_stream: typing.Optional[typing.IO[bytes]] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes an image.
        :param src: The image source.
        :param desired_type: The type to transcode to.
        :param desired_size: The desired size of the image.
        :param max_size: The maximum size of the image.
        :param greyscale: Convert the image to greyscale.
        :param out_stream: Stream to write to, if None, then a stream is returned.
        :return: The transcoded image, or None if the image cannot be transcoded.
        """


class AudioTranscoderService(ABC):
    """Provides methods for transcoding audio."""

    @abstractmethod
    async def get_audio_transcode_targets(
        self, src: model.AudioType
    ) -> typing.List[model.AudioType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """

    @abstractmethod
    async def transcode_audio(
        self,
        src: typing.IO[bytes],
        desired_type: model.AudioType,
        out_stream: typing.Optional[typing.IO[bytes]] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes audio data.
        :param src: The audio source.
        :param desired_type: The type to transcode to.
        :param out_stream: Stream to write to, if None, then a stream is returned.
        :return: The transcoded image, or None if the image cannot be transcoded.
        """


class BookTranscoderService(ABC):
    """Provides methods for transcoding books."""

    @abstractmethod
    async def get_book_transcode_targets(
        self, src: model.SourceType
    ) -> typing.List[model.SourceType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """

    @abstractmethod
    async def transcode_book(
        self,
        book_source: model.SourceProvidedBookMetadata,
        accessor: model.FileAccessor,
        desired_type: model.SourceType,
        image_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        audio_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes a book.
        :param book_source: The book to transcode.
        :param accessor: The book's accessor.
        :param desired_type: The type to transcode to.
        :param image_transcoder_options: Options to pass to the image transcoder,
          if needed.
        :param audio_transcoder_options: Options to pass to the image transcoder,
          if needed.
        :return: The transcoded book, or None if the book cannot be transcoded.
        """


class BookParserService(ABC):
    """This service parses a book and returns metadata about the book."""

    @abstractmethod
    async def can_parse(self, accessor: model.FileAccessor) -> bool:
        """
        Checks to see if the service can read this source type.
        :param accessor: The accessor.
        :return: True if it could be read, False otherwise.
        """

    @abstractmethod
    async def parse_book(self, accessor: model.FileAccessor) -> typing.Optional[dict]:
        """
        Attempt to read the given book.
        :param accessor: The accessor to read the book.
        :return: The book as a dictionary, or None if the book cannot be read.
        """

    async def enhance_accessor(
        self,
        accessor: model.FileAccessor
    ) -> typing.List[model.FileAccessor]:
        """
        Attempts to find a more specific accessor to aid processing the book.
        :param accessor: The accessor to read the book.
        :return: A list of enhanced accessors.
        """
        return []


class CacheService(ABC):
    """This service provides a way to cache binary data."""

    @abstractmethod
    async def get_stream(
        self,
        key: str,
        value_func: typing.Callable[[typing.IO[bytes]], bool],
        max_age: int = None,
    ) -> typing.IO[bytes]:
        """
        Returns a stream to the cached object.
        :param key: The key check the cache for.
        :param value_func: The function that returns the data to cache.
        :param max_age: If specified, and the existing cache entry exists and is older,
          then it is replaced.
        :return: The cached stream.
        """

    @abstractmethod
    async def cache_expired(self, key: str, max_age: int = None) -> bool:
        """
        Returns if a cached object needs to be refreshed.
        :param key: The key check the cache for.
        :param max_age: If specified, will return True if cached object is older.
        :return: True if the object needs a refresh (or does not exist), false if it is
          still relevant.
        """
