from __future__ import annotations
import logging
import typing
import asyncio
import secrets
import json
from .interfaces import (
    BackgroundService,
    BookMetadataService,
    CacheService,
    MetadataType,
    SearchResults,
    BookIndexService,
    ImageTranscoderService,
    AudioTranscoderService,
    BookTranscoderService,
    BookParserService,
    BookSourceProviderService,
    ServiceFilter,
    ALL_SERVICES_FILTER
)
from .. import plugins, config, model

logger = logging.getLogger(__name__)


def _filter_plugin_types(
    plugin_types: typing.List[str],
    load_plugins: typing.Union[bool, typing.List[str]]
):
    load_all_plugins: bool = load_plugins is True
    plugin_list: typing.List[str] = []
    if isinstance(load_plugins, list):
        plugin_list = load_plugins

    return [
        plugin_type
        for plugin_type in plugin_types
        if (load_all_plugins) or (plugin_type in plugin_list)
    ]


def get_unique_prefix(
    existing: typing.Union[
        typing.Dict[str, typing.Any],
        typing.Set[str]
    ]
):
    prefix_len = 4
    while True:
        prefix = secrets.token_hex(prefix_len)
        if prefix not in existing:
            return prefix
        prefix_len += 1


class CombinedSearchResult(
    SearchResults[MetadataType],
    typing.Generic[MetadataType]
):
    def __init__(
        self,
        separate_results: typing.Dict[str, SearchResults[MetadataType]]
    ) -> None:
        super().__init__()
        self.separate_results: typing.Dict[
            str,
            typing.Tuple[
                SearchResults[MetadataType],
                typing.AsyncGenerator[MetadataType, None]
            ]
        ] = {
            prefix: (results, results.results())
            for prefix, results in separate_results.items()
        }

    @property
    def cursor(self) -> str:
        """Returns a string representation of the cursor."""
        cursor_json = json.dumps({
            prefix: entry[0].cursor
            for prefix, entry in self.separate_results.items()
        })

        return cursor_json

    async def results(self) -> typing.AsyncGenerator[MetadataType, None]:
        """Get results iterator."""

        yielded_result = True
        while yielded_result:
            yielded_result = False
            for entry in self.separate_results.values():
                child_results, iterator = entry
                if child_results.has_next:
                    try:
                        item = await iterator.__anext__()
                        yield item
                        yielded_result = True
                    except StopAsyncIteration:
                        pass

    @property
    def has_next(self) -> bool:
        """True if there are more results available."""
        return any([
            entry[0].has_next
            for entry in self.separate_results.values()
        ])

    @property
    def at_beginning(self) -> bool:
        """True if the cursor is at the beginning of the result set."""
        return all([
            entry[0].at_beginning
            for entry in self.separate_results.values()
        ])

    @property
    def offset(self) -> int:
        """The current offset."""
        total = 0
        for child_results, _ in self.separate_results.values():
            child_offset = child_results.offset
            if child_offset < 0:
                return -1
            total += child_offset
        return total

    @property
    def remaining(self) -> int:
        """Estimate of how many are remaining."""
        total = 0
        for child_results, _ in self.separate_results.values():
            child_offset = child_results.offset
            if child_offset < 0:
                return -1
            total += child_offset
        return total

    async def skip(self, count: int) -> SearchResults[MetadataType]:
        """Fast forwards or rewinds the cursor."""
        separate_results = {
            prefix: await entry[0].skip(count)
            for prefix, entry in self.separate_results.items()
        }
        return self.__class__(separate_results)


class CombinedBookSearchResults(CombinedSearchResult[model.BookMetadata]):

    @staticmethod
    async def from_cursor_string(
        indexers: typing.Dict[str, BookIndexService],
        cursor: str
    ) -> SearchResults[model.BookMetadata]:
        cursors: typing.Dict[str, str] = json.loads(cursor)
        results = {}
        for prefix, cursor in cursors.items():
            indexer = indexers.get(prefix)
            if not indexer:
                continue
            results[prefix] = await indexer.continue_search_books(cursor)
        return CombinedBookSearchResults(results)


class CombinedPeopleSearchResults(CombinedSearchResult[model.PersonMetadata]):

    @staticmethod
    async def from_cursor_string(
        indexers: typing.Dict[str, BookIndexService],
        cursor: str
    ) -> SearchResults[model.PersonMetadata]:
        cursors: typing.Dict[str, str] = json.loads(cursor)
        results = {}
        for prefix, cursor in cursors.items():
            indexer = indexers.get(prefix)
            if not indexer:
                continue
            results[prefix] = await indexer.continue_search_people(cursor)
        return CombinedPeopleSearchResults(results)


class CombinedSeriesSearchResults(CombinedSearchResult[model.SeriesMetadata]):

    @staticmethod
    async def from_cursor_string(
        indexers: typing.Dict[str, BookIndexService],
        cursor: str
    ) -> SearchResults[model.SeriesMetadata]:
        cursors: typing.Dict[str, str] = json.loads(cursor)
        results = {}
        for prefix, cursor in cursors.items():
            indexer = indexers.get(prefix)
            if not indexer:
                continue
            results[prefix] = await indexer.continue_search_series(cursor)
        return CombinedSeriesSearchResults(results)


class ServiceRegistry(
    BookMetadataService,
    BookSourceProviderService,
    ImageTranscoderService,
    AudioTranscoderService,
    BookTranscoderService,
    BookParserService,
    CacheService,
):
    """Maintains references to services."""

    def __init__(self):
        from .simple import InMemoryBookMetadataService, NoOpCache

        self.indexers: typing.Dict[str, BookIndexService] = {}
        self.metadata: BookMetadataService = InMemoryBookMetadataService(self)
        self.source_providers: typing.List[BookSourceProviderService] = []
        self.image_transcoders: typing.List[ImageTranscoderService] = []
        self.audio_transcoders: typing.List[AudioTranscoderService] = []
        self.book_transcoders: typing.List[BookTranscoderService] = []
        self.book_parsers: typing.List[BookParserService] = []
        self.cache_service: CacheService = NoOpCache()
        self.background_services: typing.List[BackgroundService] = []

        self.source_providers_by_prefix: typing.Dict[str, BookSourceProviderService] = {}

    def __str__(self):
        return (
            "Service Registry with:\n"
            + "  indexers:           "
            + str([str(item) for item in self.indexers.values()])
            + "\n"
            + "  metadata:           "
            + str(self.metadata)
            + "\n"
            + "  source providers:   "
            + str([str(item) for item in self.source_providers])
            + "\n"
            + "  image transcoders:  "
            + str([str(item) for item in self.image_transcoders])
            + "\n"
            + "  audio transcoders:  "
            + str([str(item) for item in self.audio_transcoders])
            + "\n"
            + "  book transcoders:   "
            + str([str(item) for item in self.book_transcoders])
            + "\n"
            + "  book parsers:       "
            + str([str(item) for item in self.book_parsers])
            + "\n"
            + "  cache service:      "
            + str(self.cache_service)
            + "\n"
            + "  background service: "
            + str([str(item) for item in self.background_services])
        )

    async def initialize(
        self,
        load_plugins: typing.Union[bool, typing.List[str]] = True
    ):
        logger.debug("Starting initialization of registry...")
        logger.debug("Loading plugins...")
        existing_plugins: typing.Dict[str, typing.Any] = {}
        loaded_plugins = plugins.load_plugins(
            plugin_types=_filter_plugin_types(
                [
                    "BookMetadataService",
                    "CacheService",
                    "BookIndexService",
                    "BookSourceProviderService",
                    "ImageTranscoderService",
                    "AudioTranscoderService",
                    "BookTranscoderService",
                    "BookParserService",
                    "BackgroundService"
                ],
                load_plugins
            ),
            existing_plugins=existing_plugins,
            service_registry=self,
        )

        metadata_plugin_class = config.get("metadata.plugin", None)
        if (
            metadata_plugin_class and
            "BookMetadataService" in _filter_plugin_types(
                ["BookMetadataService"],
                load_plugins
            )
        ):
            logger.debug("Locating metadata plugin...")
            found = False
            for service in loaded_plugins.get("BookMetadataService", []):
                clazz_name = (
                    service.__class__.__module__ + "." + service.__class__.__name__
                )
                if clazz_name == metadata_plugin_class:
                    self.metadata = service
                    found = True
                    break
            if not found:
                logger.warning(
                    "Could not find metadata plugin: %s", metadata_plugin_class
                )

        cache_plugin_class = config.get("cache.plugin", None)
        if (
            cache_plugin_class and
            "CacheService" in _filter_plugin_types(
                ["CacheService"],
                load_plugins
            )
        ):
            logger.debug("Locating cache service plugin...")
            found = False
            for service in loaded_plugins.get("CacheService", []):
                clazz_name = (
                    service.__class__.__module__ + "." + service.__class__.__name__
                )
                if clazz_name == cache_plugin_class:
                    self.cache_service = service
                    found = True
                    break
            if not found:
                logger.warning(
                    "Could not find cache plugin: %s", cache_plugin_class
                )

        logger.debug("Locating any indexer plugins...")
        for indexer in loaded_plugins.get("BookIndexService", []):
            if indexer != self.metadata:
                self.indexers[get_unique_prefix(self.indexers)] = indexer
        if not self.indexers or config.get_bool("metadata.as_indexer", True):
            if isinstance(self.metadata, BookIndexService):
                self.indexers[get_unique_prefix(self.indexers)] = self.metadata
            else:
                logger.warning("Indexer not found, search unavailable.")

        logger.debug("Locating any source provider plugins...")
        for source_provider in loaded_plugins.get("BookSourceProviderService", []):
            self.source_providers.append(source_provider)
            self.source_providers_by_prefix[source_provider.get_prefix()] = \
                source_provider
            if isinstance(source_provider, BookIndexService):
                self.indexers[get_unique_prefix(self.indexers)] = source_provider

        logger.debug("Locating any image transcoder plugins...")
        for image_transcoder in loaded_plugins.get("ImageTranscoderService", []):
            self.image_transcoders.append(image_transcoder)

        logger.debug("Locating any audio transcoder plugins...")
        for audio_transcoder in loaded_plugins.get("AudioTranscoderService", []):
            self.audio_transcoders.append(audio_transcoder)

        logger.debug("Locating any book transcoder plugins...")
        for book_transcoder in loaded_plugins.get("BookTranscoderService", []):
            self.book_transcoders.append(book_transcoder)

        logger.debug("Locating any book parser plugins...")
        for book_parser in loaded_plugins.get("BookParserService", []):
            self.book_parsers.append(book_parser)

        logger.debug("Locating any background service plugins...")
        for background_service in loaded_plugins.get("BackgroundService", []):
            self.background_services.append(background_service)

        logger.debug("Initializing background services...")
        await asyncio.gather(*[
            background_service.initialize()
            for background_service in self.background_services
        ])

        logger.debug("Starting background services...")
        await asyncio.gather(*[
            background_service.start()
            for background_service in self.background_services
        ])

        logger.debug("Initialization complete!")

    async def reload(self):
        reload_functions = []
        for plugin in self.indexers.values():
            if hasattr(plugin, "reload"):
                reload_functions.append(plugin.reload())
        if hasattr(self.metadata, "reload"):
            reload_functions.append(self.metadata.reload())
        for plugin in self.source_providers:
            if hasattr(plugin, "reload"):
                reload_functions.append(plugin.reload())
        for plugin in self.image_transcoders:
            if hasattr(plugin, "reload"):
                reload_functions.append(plugin.reload())
        for plugin in self.audio_transcoders:
            if hasattr(plugin, "reload"):
                reload_functions.append(plugin.reload())
        for plugin in self.book_transcoders:
            if hasattr(plugin, "reload"):
                reload_functions.append(plugin.reload())
        for plugin in self.book_parsers:
            if hasattr(plugin, "reload"):
                reload_functions.append(plugin.reload())
        if hasattr(self.cache_service, "reload"):
            reload_functions.append(self.cache_service.reload())
        await asyncio.gather(*reload_functions)

    # BookIndexService methods
    async def search_books(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        source_types: typing.Optional[typing.List[model.SourceType]] = None,
        series_ids: typing.Optional[typing.List[str]] = None,
        person_ids: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False,
        service_filter: typing.Optional[ServiceFilter] = None
    ) -> SearchResults[model.BookMetadata]:
        """
        Searches using the given terms.
        :param terms: The search string.
        :param sort: How to sort the results.
        :param source_types: The types of books to return.
        :param series_ids: Limit the search to these series.
        :param person_ids: Limit the search to books involved with these people.
        :param include_sourceless: Include results without sources.
        :param service_filter: Optionally filter services.
        :return: The search results.
        """
        await self.refresh_source_provider()

        service_filter = service_filter or ALL_SERVICES_FILTER

        return CombinedBookSearchResults(
            {
                prefix: await indexer.search_books(
                    terms,
                    sort,
                    source_types,
                    series_ids,
                    person_ids,
                    include_sourceless
                )
                for prefix, indexer in service_filter.filter_dict(self.indexers)
            }
        )

    async def continue_search_books(
        self,
        cursor: str,
    ) -> SearchResults[model.BookMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """
        return await CombinedBookSearchResults.from_cursor_string(self.indexers, cursor)

    async def search_people(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False,
        service_filter: typing.Optional[ServiceFilter] = None
    ) -> SearchResults[model.PersonMetadata]:
        """
        Searches using the given terms.
        :param terms: The search string.
        :param sort: How to sort the results.
        :param include_sourceless: Include results without sources.
        :param service_filter: Optionally filter services.
        :return: The search results.
        """
        service_filter = service_filter or ALL_SERVICES_FILTER

        return CombinedPeopleSearchResults(
            {
                prefix: await indexer.search_people(
                    terms,
                    sort,
                    include_sourceless
                )
                for prefix, indexer in service_filter.filter_dict(self.indexers)
            }
        )

    async def continue_search_people(
        self,
        cursor: str,
    ) -> SearchResults[model.PersonMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """
        return await CombinedPeopleSearchResults.from_cursor_string(self.indexers, cursor)

    async def search_series(
        self,
        terms: typing.Optional[str] = None,
        sort: typing.Optional[typing.List[str]] = None,
        include_sourceless: bool = False,
        service_filter: typing.Optional[ServiceFilter] = None
    ) -> SearchResults[model.SeriesMetadata]:
        """
        Searches using the given terms.
        :param terms: The search string.
        :param sort: How to sort the results.
        :param include_sourceless: Include results without sources.
        :param service_filter: Optionally filter services.
        :return: The search results.
        """
        service_filter = service_filter or ALL_SERVICES_FILTER

        return CombinedSeriesSearchResults(
            {
                prefix: await indexer.search_series(
                    terms,
                    sort,
                    include_sourceless
                )
                for prefix, indexer in service_filter.filter_dict(self.indexers)
            }
        )

    async def continue_search_series(
        self,
        cursor: str,
    ) -> SearchResults[model.SeriesMetadata]:
        """
        :param cursor: The cursor string.
        :return: More search results.
        """
        return await CombinedSeriesSearchResults.from_cursor_string(self.indexers, cursor)

    # BookMetadataService methods
    async def set_book_user_metadata(self, book_id: str, **kwargs):
        """
        Updates the user provided metadata with the given values.
        :param book_id: The book id to add the user metadata to.
        :param kwargs: The metadata to update.
        """
        return await self.metadata.set_book_user_metadata(book_id, **kwargs)

    async def add_book_source(
        self, source_metadata: model.SourceProvidedBookMetadata, book_id: str = ""
    ) -> str:
        """
        Adds/updates source metadata to the metadata service.
        :param source_metadata: The metadata to add.
        :param book_id: A suggested book id.
        :return: The assigned book id.
        """
        return await self.metadata.add_book_source(source_metadata, book_id)

    async def remove_book_source(self, source_key: str):
        """
        Removes book source metadata.
        :param source_key: The source to remove.
        """
        await self.metadata.remove_book_source(source_key)

    async def add_book_scraper_source(
        self, book_id: str, scraper_metadata: model.ScraperProvidedBookMetadata
    ):
        """
        Adds/updates source metadata to the metadata service.
        :param book_id: The book id to add the scraper metadata to.
        :param scraper_metadata: The metadata to add.
        :return:
        """
        await self.metadata.add_book_scraper_source(book_id, scraper_metadata)

    async def remove_book_scraper_source(self, book_id: str, scraper_key: str):
        """
        Removes book scraper source metadata.
        :param scraper_key: The source to remove.
        """
        await self.metadata.remove_book_scraper_source(book_id, scraper_key)

    async def get_book_by_id(self, book_id: str) -> typing.Optional[model.Book]:
        """
        Returns sources for a given book.
        :param book_id: The book ID.
        :return: The sources for the given book ID.
        """
        return await self.metadata.get_book_by_id(book_id)

    async def get_book_source_metadata_by_key(
        self,
        source_key: str
    ) -> typing.Optional[model.SourceProvidedBookMetadata]:
        """
        Returns source metadata for a given source key.
        :param book_id: The source key.
        :return: The source metadata for the given source key.
        """
        return await self.metadata.get_book_source_metadata_by_key(source_key)

    async def set_series_user_metadata(self, series_id: str, **kwargs):
        """
        Updates the user provided metadata with the given values.
        :param series_id: The series id to add the user metadata to.
        :param kwargs: The metadata to update.
        """
        return await self.metadata.set_series_user_metadata(series_id, **kwargs)

    async def add_series_source(
        self, source_metadata: model.SourceProvidedSeriesMetadata, series_id: str = ""
    ) -> str:
        """
        Adds/updates series source metadata to the metadata service.
        :param source_metadata: The metadata to add.
        :param series_id: A suggested series id.
        :return: The assigned series id.
        """
        return await self.metadata.add_series_source(source_metadata, series_id)

    async def remove_series_source(self, source_key: str):
        """
        Removes book series metadata.
        :param source_key: The source to remove.
        """
        await self.metadata.remove_series_source(source_key)

    async def add_series_scraper_source(
        self, series_id: str, scraper_metadata: model.ScraperProvidedSeriesMetadata
    ):
        """
        Adds/updates series source metadata to the metadata service.
        :param series_id: The series id to add the scraper metadata to.
        :param scraper_metadata: The metadata to add.
        """
        await self.metadata.add_series_scraper_source(series_id, scraper_metadata)

    async def remove_series_scraper_source(self, series_id: str, scraper_key: str):
        """
        Removes series scraper source metadata.
        :param scraper_key: The source to remove.
        """
        await self.metadata.remove_series_scraper_source(series_id, scraper_key)

    async def get_series_metadata_by_id(
        self,
        series_id: str
    ) -> typing.Optional[model.SeriesMetadata]:
        """
        Returns series metadata for a given series ID.
        :param series_id: The series ID.
        :return: The metadata for the given series ID.
        """
        return await self.metadata.get_series_metadata_by_id(series_id)

    async def set_person_user_metadata(self, person_id: str, **kwargs):
        """
        Updates the user provided metadata with the given values.
        :param person_id: The person id to add the user metadata to.
        :param kwargs: The metadata to update.
        """
        return await self.metadata.set_person_user_metadata(person_id, **kwargs)

    async def add_person_source(
        self, source_metadata: model.SourceProvidedPersonMetadata, person_id: str = ""
    ) -> str:
        """
        Adds/updates person source metadata to the metadata service.
        :param source_metadata: The metadata to add.
        :param person_id: A suggested person id.
        :return: The assigned person id.
        """
        return await self.metadata.add_person_source(source_metadata, person_id)

    async def remove_person_source(self, source_key: str):
        """
        Removes person source metadata.
        :param source_key: The source to remove.
        """
        await self.metadata.remove_person_source(source_key)

    async def add_person_scraper_source(
        self, person_id: str, scraper_metadata: model.ScraperProvidedPersonMetadata
    ):
        """
        Adds/updates person source metadata to the metadata service.
        :param person_id: The person id to add the scraper metadata to.
        :param scraper_metadata: The metadata to add.
        """
        await self.metadata.add_person_scraper_source(person_id, scraper_metadata)

    async def remove_person_scraper_source(self, person_id: str, scraper_key: str):
        """
        Removes person scraper source metadata.
        :param scraper_key: The source to remove.
        """
        await self.metadata.remove_person_scraper_source(person_id, scraper_key)

    async def get_person_metadata_by_id(
        self,
        person_id: str
    ) -> typing.Optional[model.PersonMetadata]:
        """
        Returns person metadata for a given person ID.
        :param person_id: The person ID.
        :return: The metadata for the given person ID.
        """
        return await self.metadata.get_person_metadata_by_id(person_id)

    # BookSourceProviderService methods
    async def refresh_source_provider(self):
        """
        Refreshes the registered source providers.
        """
        await asyncio.gather(*[
            source_provider.refresh_source_provider()
            for source_provider in self.source_providers
        ])

    def get_prefix(self) -> str:
        """Returns the source prefix for this source."""
        return ""

    async def get_book_source(
        self,
        source_metadata: model.SourceProvidedBookMetadata
    ) -> typing.Optional[model.BookSource]:
        """
        Gets a book source for the given book.
        :param source_metadata: The source metadata.
        :return: The book's source, or None if it cannot be found.
        """
        source_key = source_metadata.source_key
        pos = source_key.find(":")
        prefix = source_key[:pos]
        source_provider = self.source_providers_by_prefix.get(prefix)
        if source_provider:
            return await source_provider.get_book_source(source_metadata)

        return None

    async def get_book_source_by_key(
        self,
        source_key: str
    ) -> typing.Optional[model.BookSource]:
        """
        Gets a book source for the source key
        :param source_key: The source key.
        :return: The book's source, or None if it cannot be found.
        """
        pos = source_key.find(":")
        prefix = source_key[:pos]
        source_provider = self.source_providers_by_prefix.get(prefix)
        if source_provider:
            return await source_provider.get_book_source_by_key(source_key)
        return None

    # ImageTranscoderService methods
    async def get_image_transcode_targets(
        self, src: model.ImageType
    ) -> typing.List[model.ImageType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """
        all_results = set()
        for targets in await asyncio.gather(*[
            image_transcoder.get_image_transcode_targets(src)
            for image_transcoder in self.image_transcoders
        ]):
            for result in targets:
                all_results.add(result)
        return list(all_results)

    async def transcode_image(
        self,
        src: typing.IO[bytes],
        desired_type: model.ImageType,
        desired_size: typing.Tuple[int, int] = None,
        max_size: typing.Tuple[int, int] = None,
        greyscale: bool = None,
        out_stream: typing.Optional[typing.IO[bytes]] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes an image.
        :param src: The image source.
        :param desired_type: The type to transcode to.
        :param desired_size: The desired size of the image.
        :param max_size: The maximum size of the image.
        :param greyscale: Convert the image to greyscale.
        :param out_stream: Stream to write to, if None, then a stream is returned.
        :return: The transcoded image, or None if the image cannot be transcoded.
        """
        for image_transcoder in self.image_transcoders:
            ret = await image_transcoder.transcode_image(
                src=src,
                desired_type=desired_type,
                desired_size=desired_size,
                max_size=max_size,
                greyscale=greyscale,
                out_stream=out_stream,
                **kwargs
            )
            if ret is not None:
                return ret
        return None

    # AudioTranscoderService
    async def get_audio_transcode_targets(
        self, src: model.AudioType
    ) -> typing.List[model.AudioType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """
        all_results = set()
        for targets in await asyncio.gather(*[
            audio_transcoder.get_audio_transcode_targets(src)
            for audio_transcoder in self.audio_transcoders
        ]):
            for result in targets:
                all_results.add(result)
        return list(all_results)

    async def transcode_audio(
        self,
        src: typing.IO[bytes],
        desired_type: model.AudioType,
        out_stream: typing.Optional[typing.IO[bytes]] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes audio data.
        :param src: The audio source.
        :param desired_type: The type to transcode to.
        :param out_stream: Stream to write to, if None, then a stream is returned.
        :return: The transcoded image, or None if the image cannot be transcoded.
        """
        for audio_transcoder in self.audio_transcoders:
            ret = await audio_transcoder.transcode_audio(
                src=src,
                desired_type=desired_type,
                out_stream=out_stream,
                **kwargs
            )
            if ret is not None:
                return ret
        return None

    # BookTranscoderService methods
    async def get_book_transcode_targets(
        self, src: model.SourceType
    ) -> typing.List[model.SourceType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """
        all_results = set()

        tasks = [
            asyncio.ensure_future(book_transcoder.get_book_transcode_targets(src))
            for book_transcoder in self.book_transcoders
        ]

        for targets in await asyncio.gather(*tasks):
            for result in targets:
                all_results.add(result)
        return list(all_results)

    async def transcode_book(
        self,
        book_source: model.SourceProvidedBookMetadata,
        accessor: model.FileAccessor,
        desired_type: model.SourceType,
        image_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        audio_transcoder_options: typing.Optional[typing.Dict[str, typing.Any]] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes a book.
        :param book_source: The book to transcode.
        :param accessor: The book's accessor.
        :param desired_type: The type to transcode to.
        :param image_transcoder_options: Options to pass to the image transcoder, if
          needed.
        :return: The transcoded book, or None if the book cannot be transcoded.
        """
        image_transcoder_options = image_transcoder_options or {}
        for book_transcoder in self.book_transcoders:
            ret = await book_transcoder.transcode_book(
                book_source=book_source,
                accessor=accessor,
                desired_type=desired_type,
                image_transcoder_options=image_transcoder_options,
                audio_transcoder_options=audio_transcoder_options,
                **kwargs
            )
            if ret is not None:
                return ret
        return None

    # BookParserService methods
    async def can_parse(self, accessor: model.FileAccessor) -> bool:
        """
        Checks to see if the service can read this source type.
        :param accessor: The accessor.
        :return: True if it could be read, False otherwise.
        """
        for book_parser in self.book_parsers:
            ret = await book_parser.can_parse(accessor)
            if ret:
                return ret
        return False

    async def parse_book(self, accessor: model.FileAccessor) -> typing.Optional[dict]:
        """
        Attempt to read the given book.
        :param accessor: The accessor to read the book.
        :return: The book as a dictionary, or None if the book cannot be read.
        """
        for book_parser in self.book_parsers:
            ret = await book_parser.parse_book(accessor)
            if ret is not None:
                return ret
        return None

    async def enhance_accessor(
        self,
        accessor: model.FileAccessor
    ) -> typing.List[model.FileAccessor]:
        """
        Attempts to find a more specific accessor to aid processing the book.
        :param accessor: The accessor to read the book.
        :return: A list of enhanced accessors.
        """
        enhanced_accessors: typing.List[model.FileAccessor] = []
        for book_parser in self.book_parsers:
            if await book_parser.can_parse(accessor):
                enhanced_accessors += await book_parser.enhance_accessor(accessor)
        return enhanced_accessors

    # CacheService methods
    async def get_stream(
        self,
        key: str,
        value_func: typing.Callable[[typing.IO[bytes]], bool],
        max_age: int = None,
    ) -> typing.IO[bytes]:
        """
        Returns a stream to the cached object.
        :param key: The key check the cache for.
        :param value_func: The function that returns the data to cache.
        :param max_age: If specified, and the existing cache entry exists and is older,
          then it is replaced.
        :return: The cached stream.
        """
        return await self.cache_service.get_stream(key, value_func, max_age)

    async def cache_expired(self, key: str, max_age: int = None) -> bool:
        """
        Returns if a cached object needs to be refreshed.
        :param key: The key check the cache for.
        :param max_age: If specified, will return True if cached object is older.
        :return: True if the object needs a refresh (or does not exist),
          false if it is still relevant.
        """
        return await self.cache_service.cache_expired(key, max_age)
