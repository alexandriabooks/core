import typing
import logging
from pkg_resources import iter_entry_points

logger = logging.getLogger(__name__)
__ENUMERATED_PLUGINS__: typing.Dict[str, typing.List[typing.Any]] = {}
__CACHED_PLUGINS__: typing.Dict[str, typing.List[typing.Any]] = {}


def enumerate_plugins():
    if not __ENUMERATED_PLUGINS__:
        plugins = {}
        for entry_point in iter_entry_points(
            group="alexandriabooks_plugins", name=None
        ):
            try:
                plugins_module = entry_point.load()
                if "ALEXANDRIA_PLUGINS" in dir(plugins_module):
                    for (
                        plugin_type,
                        plugins_list,
                    ) in plugins_module.ALEXANDRIA_PLUGINS.items():
                        found_plugins = plugins.setdefault(plugin_type, [])
                        for plugin in plugins_list:
                            found_plugins.append(plugin)
            except BaseException as exp:
                logger.warning(
                    "Could not load %s, skipping plugin [%s]",
                    entry_point,
                    exp,
                    exc_info=exp,
                )
        __ENUMERATED_PLUGINS__.update(plugins)
    return __ENUMERATED_PLUGINS__


def load_plugins(
    plugin_types: typing.Optional[typing.List[str]] = None,
    existing_plugins: typing.Optional[typing.Dict[str, typing.Any]] = None,
    **kwargs
):
    if existing_plugins is None:
        existing_plugins = {}

    for plugin_type, plugins_list in enumerate_plugins().items():
        if plugin_types is not None and plugin_type not in plugin_types:
            continue
        if plugin_type in __CACHED_PLUGINS__:
            continue
        created_plugins = []
        for plugin_class in plugins_list:
            if plugin_class in existing_plugins:
                logger.debug(f"Caught duplicate instance of {plugin_class}")
                created_plugins.append(existing_plugins.get(plugin_class))
                continue

            try:
                instance = plugin_class(**kwargs)
                existing_plugins[plugin_class] = instance
                created_plugins.append(instance)
            except BaseException as exp:
                logger.warning(
                    "Could not create %s, skipping plugin [%s]",
                    plugin_class,
                    exp,
                    exc_info=exp,
                )
        __CACHED_PLUGINS__[plugin_type] = created_plugins
    return __CACHED_PLUGINS__
