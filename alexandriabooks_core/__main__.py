import logging
import typing

import click
from .services import ServiceRegistry
from .plugins import load_plugins


# Mostly borrowed from click-log
class ClickHandler(logging.Handler):
    _use_stderr = True

    def emit(self, record):
        try:
            msg = self.format(record)
            click.echo(msg, err=self._use_stderr)
        except Exception:
            self.handleError(record)


# Mostly borrowed from click-log
class ClickColorFormatter(logging.Formatter):
    colors = {
        'error': dict(fg='red'),
        'exception': dict(fg='red'),
        'critical': dict(fg='red'),
        'debug': dict(fg='blue'),
        'warning': dict(fg='yellow')
    }

    def format(self, record):
        if not record.exc_info:
            level = record.levelname.lower()
            msg = logging.Formatter.format(self, record)

            if level in self.colors:
                prefix = click.style(
                    '{}: '.format(level),
                    **self.colors[level]
                )
                msg = '\n'.join(prefix + x for x in msg.splitlines())
            return msg
        return logging.Formatter.format(self, record)


log_handler = ClickHandler()

logging.basicConfig(handlers=[log_handler])


@click.group()
@click.option(
    "--verbosity",
    "-v",
    help="Sets the global logging verbosity",
    default="INFO",
    metavar="LEVEL"
)
@click.option(
    "--log-format",
    help="Sets the logging format",
    default="%(name)s:%(message)s",
    metavar="LOG_FORMAT"
)
@click.option(
    "--logger-level",
    help="Sets the logging format for a specific logger",
    nargs=2,
    multiple=True,
    metavar="LOGGER_NAME LEVEL"
)
def cli(
    verbosity: str,
    log_format: typing.Optional[str] = None,
    logger_level: typing.List[typing.Tuple[str, str]] = []
):
    logging.root.setLevel(verbosity.upper())
    log_format = log_format or "%(name)s:%(message)s"
    log_handler.setFormatter(
        ClickColorFormatter(
            log_format
        )
    )

    if logger_level:
        for logger_name, level in logger_level:
            logging.getLogger(logger_name).setLevel(level.upper())


@cli.command()
def run():
    """Runs the AlexandriaBooks service."""
    import asyncio
    loop = asyncio.get_event_loop()

    service_registry = ServiceRegistry()
    loop.create_task(service_registry.initialize()).add_done_callback(
        lambda task: logging.getLogger(
            "alexandriabooks_core.services.registry"
        ).info("Created service registry:\n%s", service_registry)
    )

    loop.run_forever()


@cli.command()
@click.argument('terms', nargs=-1)
def search(terms):
    """Initializes AlexandriaBooks service and runs a search."""
    import asyncio

    async def search_main():
        service_registry = ServiceRegistry()
        await service_registry.initialize()
        for result in await service_registry.search_books(" ".join(terms)):
            print(result)

    asyncio.run(search_main())


load_plugins(plugin_types=["CliCommand"], cli_root=cli)

if __name__ == "__main__":
    try:
        import uvloop
        uvloop.install()
    except ImportError:
        pass
    cli()
