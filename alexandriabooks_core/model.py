from __future__ import annotations
import typing
import io
import base64
import datetime
from enum import Enum
from abc import ABC, abstractmethod
from .utils import (
    ensure_list,
    ensure_dict,
    ensure_date,
    ensure_number,
    ensure_string,
    ensure_bool
)

import nameparser


class ImageType(Enum):
    """Holds the different image types."""

    JPEG = 1
    PNG = 2
    GIF = 3
    BMP = 4
    TIFF = 5
    WEBP = 6

    def ext(self):
        """The file extension for the source type."""
        return self.name.lower()

    def extra_exts(self):
        if self == ImageType.JPEG:
            return ["jpg"]
        if self == ImageType.TIFF:
            return ["tif"]
        return []

    def matches_file(self, filename):
        """Checks the passed filename to see if the extension matches."""
        normalized = filename.lower()

        if normalized.endswith("." + self.ext()):
            return True

        for extra_ext in self.extra_exts():
            if normalized.endswith("." + extra_ext):
                return True

        return False

    def get_mimetype(self):
        """Returns the mime type for the given type."""
        if self == ImageType.JPEG:
            return "image/jpeg"
        elif self == ImageType.PNG:
            return "image/png"
        elif self == ImageType.GIF:
            return "image/gif"
        elif self == ImageType.BMP:
            return "image/bmp"
        elif self == ImageType.TIFF:
            return "image/tiff"
        elif self == ImageType.WEBP:
            return "image/webp"
        else:
            return "application/octet-stream"

    @staticmethod
    def to_image_type(image_type):
        if image_type is None:
            return None
        try:
            if isinstance(image_type, str):
                return ImageType[image_type.upper()]
            return ImageType(image_type)
        except (ValueError, KeyError):
            return ImageType.find_image_type(f"image.{image_type}")

    @staticmethod
    def find_image_type(filename):
        for image_type in ImageType:
            if image_type.matches_file(filename):
                return image_type
        return None


class AudioType(Enum):
    """Holds the different audio types."""

    MP3 = 1
    FLAC = 2
    M4A = 3
    OGG = 4
    WMA = 5
    WAV = 6

    def ext(self):
        """The file extension for the source type."""
        return self.name.lower()

    def extra_exts(self):
        if self == AudioType.M4A:
            return ["m4b", "mp4"]
        return []

    def matches_file(self, filename):
        """Checks the passed filename to see if the extension matches."""
        normalized = filename.lower()

        if normalized.endswith("." + self.ext()):
            return True

        for extra_ext in self.extra_exts():
            if normalized.endswith("." + extra_ext):
                return True

        return False

    def get_mimetype(self):
        """Returns the mime type for the given type."""
        if self == SourceType.MP3:
            return "audio/mpeg"
        elif self == SourceType.FLAC:
            return "audio/flac"
        elif self == SourceType.M4A:
            return "audio/mp4"
        elif self == SourceType.OGG:
            return "audio/ogg"
        elif self == SourceType.WMA:
            return "audio/x-ms-wma"
        elif self == SourceType.WAV:
            return "audio/wav"
        else:
            return "application/octet-stream"

    @staticmethod
    def to_audio_type(audio_type):
        if audio_type is None:
            return None
        try:
            if isinstance(audio_type, str):
                return AudioType[audio_type]
            return AudioType(audio_type)
        except ValueError:
            return None

    @staticmethod
    def find_audio_type(filename):
        for audio_type in AudioType:
            if audio_type.matches_file(filename):
                return audio_type
        return None


class SourceType(Enum):
    """Holds the different source types."""

    EPUB = 1
    CBR = 2
    CBZ = 3
    CBT = 4
    CB7 = 5
    CBA = 6
    PDF = 7
    MOBI = 8

    MP3 = 50
    FLAC = 51
    M4A = 52
    OGG = 53
    WMA = 54
    WAV = 55

    ZIP_MP3 = 150
    ZIP_FLAC = 151
    ZIP_M4A = 152
    ZIP_OGG = 153
    ZIP_WMA = 154
    ZIP_WAV = 155

    def ext(self):
        """The file extension for the source type."""
        if self.name.lower().startswith("zip"):
            return "zip"
        return self.name.lower()

    def extra_exts(self):
        if self == SourceType.MOBI:
            return ["prc"]
        if self == SourceType.M4A:
            return ["m4b", "mp4"]
        return []

    def matches_file(self, filename):
        """Checks the passed filename to see if the extension matches."""
        normalized = filename.lower()

        if normalized.endswith(".zip"):
            return False

        if normalized.endswith("." + self.ext()):
            return True

        for extra_ext in self.extra_exts():
            if normalized.endswith("." + extra_ext):
                return True

        return False

    def get_mimetype(self):
        """Returns the mime type for the given type."""
        if self == SourceType.EPUB:
            return "application/epub+zip"
        elif self == SourceType.CBR:
            return "application/x-cbr"
        elif self == SourceType.CBZ:
            return "application/x-cbz"
        elif self == SourceType.CBT:
            return "application/x-cbt"
        elif self == SourceType.CB7:
            return "application/x-cb7"
        elif self == SourceType.CBA:
            return "application/x-cba"
        elif self == SourceType.PDF:
            return "application/pdf"
        elif self == SourceType.MOBI:
            return "application/x-mobipocket-ebook"
        elif self == SourceType.MP3:
            return "audio/mpeg"
        elif self == SourceType.FLAC:
            return "audio/flac"
        elif self == SourceType.M4A:
            return "audio/mp4"
        elif self == SourceType.OGG:
            return "audio/ogg"
        elif self == SourceType.WMA:
            return "audio/x-ms-wma"
        elif self == SourceType.WAV:
            return "audio/wav"
        elif self.name.lower().startswith("zip"):
            return "application/zip"
        else:
            return "application/octet-stream"

    @staticmethod
    def to_source_type(source_type):
        if source_type is None:
            return None
        if isinstance(source_type, str):
            return SourceType[source_type]
        return SourceType(source_type)

    @staticmethod
    def find_source_type(filename):
        for source_type in SourceType:
            if source_type.matches_file(filename):
                return source_type
        return None


class MetadataImage:
    def __init__(
        self,
        image_type: ImageType,
        data: typing.Optional[bytes] = None,
        filepath: typing.Optional[str] = None
    ):
        super().__init__()
        assert image_type is not None
        self.image_type = image_type
        self.data = data
        self.filepath = filepath

    def as_stream(self):
        if self.data is not None:
            return io.BytesIO(self.data)
        elif self.filepath is not None:
            return open(self.filepath, "r")
        return None

    def as_bytes(self):
        if self.data is not None:
            return self.data
        elif self.filepath is not None:
            with open(self.filepath, "r") as handle:
                data = handle.read()
            return data

    def as_dict(self):
        result = {
            "image_type": self.image_type.name
        }
        if self.data is not None:
            result["data"] = base64.b64encode(self.data).decode("ascii")
        if self.filepath is not None:
            result["filepath"] = self.filepath
        return result

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None
        loaded = entry.copy()
        loaded["image_type"] = ImageType.to_image_type(loaded["image_type"])
        if "data" in entry:
            loaded["data"] = base64.b64decode(entry["data"])
        return MetadataImage(**loaded)


class Tag:
    def __init__(self, **kwargs):
        self.key = kwargs["key"]
        self.value = kwargs.get("value", None)

    def __repr__(self):
        return "{}(key={}, value={})".format(
            type(self).__name__, repr(self.key), repr(self.value)
        )


class BaseMetadata:
    def __init__(self, **kwargs):
        self.set_metadata(**kwargs)

    @property
    def _default_tags(self):
        return self._default_list

    @property
    def _default_list(self):
        return []

    @property
    def _default_dict(self):
        return {}

    @property
    def _default_date(self):
        return None

    @property
    def _default_number(self):
        return None

    @property
    def _default_string(self):
        return None

    @property
    def _default_bool(self):
        return False

    def _set_tags(
        self,
        metadata: typing.Dict[str, typing.Any],
        key: str,
        dest_key: typing.Optional[str] = None
    ):
        dest_key = dest_key or key
        if key not in metadata and hasattr(self, dest_key):
            return

        value = metadata.get(key, self._default_tags)
        if value is not None:
            tags: typing.List[Tag] = []
            if isinstance(value, dict):
                for tag_key, tag_value in value.items():
                    tags.append(Tag(key=tag_key, value=tag_value))
            elif isinstance(value, (list, tuple)):
                for item in value:
                    if not item:
                        continue
                    if isinstance(item, dict):
                        tags.append(Tag(
                            key=item["key"],
                            value=item.get("value", None)
                        ))
                    elif isinstance(item, (tuple, list)):
                        if len(item) > 1:
                            tags.append(Tag(key=item[0], value=item[1]))
                        else:
                            tags.append(Tag(key=item[0], value=item))
                    else:
                        tags.append(Tag(key=item.key, value=item.value))
            setattr(self, dest_key, tags)
        else:
            setattr(self, dest_key, value)

    def _set_list(
        self,
        metadata: typing.Dict[str, typing.Any],
        key: str,
        dest_key: typing.Optional[str] = None
    ):
        dest_key = dest_key or key
        if key not in metadata and hasattr(self, dest_key):
            return

        value = metadata.get(key)
        setattr(self, dest_key, ensure_list(value, self._default_list))

    def _set_dict(
        self,
        metadata: typing.Dict[str, typing.Any],
        key: str,
        dest_key: typing.Optional[str] = None
    ):
        dest_key = dest_key or key
        if key not in metadata and hasattr(self, dest_key):
            return

        value = metadata.get(key)
        setattr(self, dest_key, ensure_dict(value, self._default_dict))

    def _set_date(
        self,
        metadata: typing.Dict[str, typing.Any],
        key: str,
        dest_key: typing.Optional[str] = None
    ):
        dest_key = dest_key or key
        if key not in metadata and hasattr(self, dest_key):
            return

        value = metadata.get(key)
        setattr(self, dest_key, ensure_date(value, self._default_date))

    def _set_number(
        self,
        metadata: typing.Dict[str, typing.Any],
        key: str,
        dest_key: typing.Optional[str] = None
    ):
        dest_key = dest_key or key
        if key not in metadata and hasattr(self, dest_key):
            return

        value = metadata.get(key)
        setattr(self, dest_key, ensure_number(value, self._default_number))

    def _set_string(
        self,
        metadata: typing.Dict[str, typing.Any],
        key: str,
        dest_key: typing.Optional[str] = None
    ):
        dest_key = dest_key or key
        if key not in metadata and hasattr(self, dest_key):
            return

        value = metadata.get(key)
        setattr(self, dest_key, ensure_string(value, self._default_string))

    def _set_bool(
        self,
        metadata: typing.Dict[str, typing.Any],
        key: str,
        dest_key: typing.Optional[str] = None
    ):
        dest_key = dest_key or key
        if key not in metadata and hasattr(self, dest_key):
            return

        value = metadata.get(key)
        setattr(self, dest_key, ensure_bool(value, self._default_bool))

    def _set_image(
        self,
        metadata: typing.Dict[str, typing.Any],
        key: str,
        dest_key: typing.Optional[str] = None
    ):
        dest_key = dest_key or key
        if key not in metadata and hasattr(self, dest_key):
            return

        value = metadata.get(key)
        if value is not None:
            if isinstance(value, tuple) and len(value) >= 2:
                image_type, image_data = value[:2]
                image_type = ImageType.to_image_type(image_type)
                if isinstance(image_data, bytes):
                    value = MetadataImage(image_type, data=image_data)
                elif isinstance(image_data, str):
                    value = MetadataImage(image_type, filepath=image_data)
            elif isinstance(value, dict):
                image_type = ImageType.to_image_type(value.get("image_type"))
                image_data = value.get("data")
                filepath = value.get("filepath")

                if image_type and (image_data or filepath):
                    value = MetadataImage(
                        image_type,
                        data=image_data,
                        filepath=filepath
                    )
            if isinstance(value, MetadataImage):
                setattr(self, dest_key, value)
        else:
            setattr(self, dest_key, value)

    def as_dict(self):
        """Returns the metadata as a dictonary"""
        return {}

    def set_metadata(self, **kwargs):
        """Sets the metadata from args."""
        pass

    def __repr__(self):
        return "{}({})".format(
            type(self).__name__,
            ", ".join(
                f"{param}={repr(value)}" for param, value in self.as_dict().items()
            ),
        )


class BaseBookMetadata(BaseMetadata):
    """Represents a single book record."""

    identifiers: typing.Dict[str, str]
    title: str
    subtitle: str
    publisher: str
    rights: str
    publish_date: datetime.date
    language: typing.List[str]
    country: str
    age: int
    description: str
    genres: typing.List[str]
    tags: typing.List[Tag]
    acquisition_date: datetime.date

    front_cover: MetadataImage
    back_cover: MetadataImage

    series: typing.List[SeriesEntryMetadata]
    reading_lists: typing.List[ReadingListEntryMetadata]

    writers: typing.List[str]
    artists: typing.List[str]
    coverartists: typing.List[str]
    letterers: typing.List[str]
    colorists: typing.List[str]
    inkers: typing.List[str]
    pencillers: typing.List[str]
    editors: typing.List[str]
    translators: typing.List[str]
    contributors: typing.List[str]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def set_metadata(self, **kwargs):
        self._set_dict(kwargs, "identifiers")
        self._set_string(kwargs, "title")
        self._set_string(kwargs, "subtitle")
        self._set_string(kwargs, "publisher")
        self._set_string(kwargs, "rights")
        self._set_date(kwargs, "publish_date")
        self._set_list(kwargs, "language")
        self._set_string(kwargs, "country")
        self._set_number(kwargs, "age")
        self._set_string(kwargs, "description")
        self._set_list(kwargs, "genres")
        self._set_tags(kwargs, "tags")
        self._set_date(kwargs, "acquisition_date")

        self._set_image(kwargs, "front_cover")
        self._set_image(kwargs, "back_cover")

        self._set_list(kwargs, "series")
        self._set_list(kwargs, "reading_lists")

        self._set_list(kwargs, "writers")
        self._set_list(kwargs, "artists")
        self._set_list(kwargs, "coverartists")
        self._set_list(kwargs, "letterers")
        self._set_list(kwargs, "colorists")
        self._set_list(kwargs, "inkers")
        self._set_list(kwargs, "pencillers")
        self._set_list(kwargs, "pencilers", "pencillers")
        self._set_list(kwargs, "editors")
        self._set_list(kwargs, "translators")
        self._set_list(kwargs, "contributors")

    @property
    def people(self) -> typing.List[str]:
        all_people = []

        for person in self.writers:
            if person not in all_people:
                all_people.append(person)

        for person in self.artists:
            if person not in all_people:
                all_people.append(person)

        for person in self.coverartists:
            if person not in all_people:
                all_people.append(person)

        for person in self.letterers:
            if person not in all_people:
                all_people.append(person)

        for person in self.colorists:
            if person not in all_people:
                all_people.append(person)

        for person in self.inkers:
            if person not in all_people:
                all_people.append(person)

        for person in self.pencillers:
            if person not in all_people:
                all_people.append(person)

        for person in self.editors:
            if person not in all_people:
                all_people.append(person)

        for person in self.translators:
            if person not in all_people:
                all_people.append(person)

        for person in self.contributors:
            if person not in all_people:
                all_people.append(person)

        return all_people

    def get_tags(self, tag_key):
        matching_tags = []
        if self.tags:
            for tag in self.tags:
                if tag.key == tag_key:
                    matching_tags.append(tag)
        return matching_tags

    def __str__(self):
        return "'{}' by '{}' ({})".format(self.title, self.people, self.publish_date)

    def as_dict(self):
        """Returns the book as a dictonary"""

        if self.front_cover:
            front_cover = self.front_cover.as_dict()
        else:
            front_cover = None

        if self.back_cover:
            back_cover = self.back_cover.as_dict()
        else:
            back_cover = None

        series = []
        if self.series:
            series = [
                series.as_dict()
                for series in self.series
            ]

        tags = {}
        if self.tags:
            tags = {
                tag.key: tag.value
                for tag in self.tags
            }

        return {
            "identifiers": self.identifiers,
            "title": self.title,
            "subtitle": self.subtitle,
            "publisher": self.publisher,
            "rights": self.rights,
            "publish_date": str(self.publish_date)
            if self.publish_date else None,
            "language": self.language,
            "country": self.country,
            "age": self.age,
            "description": self.description,
            "genres": self.genres,
            "tags": tags,
            "acquisition_date": str(self.acquisition_date)
            if self.acquisition_date else None,
            "front_cover": front_cover,
            "back_cover": back_cover,
            "series": series,
            "writers": self.writers,
            "artists": self.artists,
            "coverartists": self.coverartists,
            "letterers": self.letterers,
            "colorists": self.colorists,
            "inkers": self.inkers,
            "pencillers": self.pencillers,
            "editors": self.editors,
            "translators": self.translators,
            "contributors": self.contributors,
        }

    @staticmethod
    def load_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        loaded = entry.copy()
        if "front_cover" in entry:
            loaded["front_cover"] = MetadataImage.from_dict(
                entry["front_cover"]
            )
        if "back_cover" in entry:
            loaded["front_cover"] = MetadataImage.from_dict(
                entry["front_cover"]
            )
        if "series" in entry:
            loaded_series = [
                SeriesEntryMetadata.from_dict(series_entry)
                for series_entry in entry["series"]
            ]
            loaded["series"] = loaded_series
        if "reading_lists" in entry:
            loaded_reading_lists = [
                ReadingListEntryMetadata.from_dict(reading_list_entry)
                for reading_list_entry in entry["reading_lists"]
            ]
            loaded["reading_lists"] = loaded_reading_lists
        return loaded


class BookMetadata:

    identifiers: typing.Dict[str, str]
    title: str
    subtitle: str
    publisher: str
    rights: str
    publish_date: datetime.date
    language: typing.List[str]
    country: str
    age: int
    description: str
    genres: typing.List[str]
    tags: typing.List[Tag]
    acquisition_date: datetime.date

    front_cover: MetadataImage
    back_cover: MetadataImage

    series: typing.List[SeriesEntryMetadata]
    reading_lists: typing.List[ReadingListEntryMetadata]

    writers: typing.List[str]
    artists: typing.List[str]
    coverartists: typing.List[str]
    letterers: typing.List[str]
    colorists: typing.List[str]
    inkers: typing.List[str]
    pencillers: typing.List[str]
    editors: typing.List[str]
    translators: typing.List[str]
    contributors: typing.List[str]

    def __init__(
        self,
        book_id: str,
        user_metadata: typing.Optional[UserProvidedBookMetadata] = None,
        source_metadata: typing.Optional[
            typing.Dict[str, SourceProvidedBookMetadata]
        ] = None,
        scraper_metadata: typing.Optional[
            typing.Dict[str, ScraperProvidedBookMetadata]
        ] = None
    ):
        self.book_id = book_id
        if not self.book_id:
            raise Exception("BookMetadata missing book_id")

        if user_metadata is None:
            user_metadata = UserProvidedBookMetadata(book_id)

        self.user_metadata: UserProvidedBookMetadata = user_metadata

        self.source_metadata: typing.Dict[
            str,
            SourceProvidedBookMetadata
        ] = source_metadata or {}

        self.scraper_metadata: typing.Dict[
            str,
            ScraperProvidedBookMetadata
        ] = scraper_metadata or {}

    def __str__(self):
        return "'{}' by '{}' ({})".format(self.title, self.people, self.publish_date)

    def __repr__(self):
        return "{}({})".format(
            type(self).__name__,
            ", ".join(
                f"{param}={repr(value)}" for param, value in self.as_dict().items()
            ),
        )

    def __getattr__(self, name):
        value = getattr(self.user_metadata, name)
        if value or value == 0:
            return value

        values = None
        for metadata in self.scraper_metadata.values():
            value = getattr(metadata, name)
            if value or value == 0:
                if isinstance(value, list):
                    values = value.copy()
                else:
                    return value

        for metadata in self.source_metadata.values():
            value = getattr(metadata, name)
            if value or value == 0:
                if values:
                    return values + value
                return value

        return value

    @property
    def people(self) -> typing.List[str]:
        all_people = []

        for person in self.writers:
            if person not in all_people:
                all_people.append(person)

        for person in self.artists:
            if person not in all_people:
                all_people.append(person)

        for person in self.coverartists:
            if person not in all_people:
                all_people.append(person)

        for person in self.letterers:
            if person not in all_people:
                all_people.append(person)

        for person in self.colorists:
            if person not in all_people:
                all_people.append(person)

        for person in self.inkers:
            if person not in all_people:
                all_people.append(person)

        for person in self.pencillers:
            if person not in all_people:
                all_people.append(person)

        for person in self.editors:
            if person not in all_people:
                all_people.append(person)

        for person in self.translators:
            if person not in all_people:
                all_people.append(person)

        for person in self.contributors:
            if person not in all_people:
                all_people.append(person)

        return all_people

    def as_dict(self):
        ret = {
            "book_id": self.book_id,
            "source_metadata": {
                key: metadata.as_dict()
                for key, metadata in self.source_metadata.items()
            },
            "scraper_metadata": {
                key: metadata.as_dict()
                for key, metadata in self.scraper_metadata.items()
            },
        }
        if self.user_metadata:
            ret["user_metadata"] = self.user_metadata.as_dict()
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        book_id = entry["book_id"]
        source_metadata = {
            key: SourceProvidedBookMetadata.from_dict(metadata)
            for key, metadata in entry.get("source_metadata", {}).items()
        }
        scraper_metadata = {
            key: ScraperProvidedBookMetadata.from_dict(metadata)
            for key, metadata in entry.get("scraper_metadata", {}).items()
        }
        if "user_metadata" in entry:
            user_metadata = UserProvidedBookMetadata.from_dict(
                book_id,
                entry["user_metadata"]
            )
        else:
            user_metadata = None

        return BookMetadata(
            book_id=book_id,
            source_metadata=source_metadata,
            scraper_metadata=scraper_metadata,
            user_metadata=user_metadata
        )


class UserProvidedBookMetadata(BaseBookMetadata):
    def __init__(self, book_id: str, **kwargs):
        super().__init__(**kwargs)
        self.book_id = book_id

    @property
    def _default_list(self):
        return None

    @property
    def _default_dict(self):
        return None

    @property
    def people(self) -> typing.List[str]:
        all_people = []

        if self.writers:
            for person in self.writers:
                if person not in all_people:
                    all_people.append(person)

        if self.artists:
            for person in self.artists:
                if person not in all_people:
                    all_people.append(person)

        if self.editors:
            for person in self.editors:
                if person not in all_people:
                    all_people.append(person)

        if self.translators:
            for person in self.translators:
                if person not in all_people:
                    all_people.append(person)

        if self.contributors:
            for person in self.contributors:
                if person not in all_people:
                    all_people.append(person)

        return all_people

    def as_dict(self):
        ret = super().as_dict()
        return ret

    @staticmethod
    def from_dict(book_id: str, entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return UserProvidedBookMetadata(
            book_id,
            **BaseBookMetadata.load_dict(entry)
        )


class SourceProvidedBookMetadata(BaseBookMetadata):
    def __init__(self, source_key, source_type, **kwargs):
        super().__init__(**kwargs)

        self.source_key = source_key
        self.source_type = SourceType.to_source_type(source_type)

    def as_dict(self):
        ret = super().as_dict()
        ret.update(
            {
                "source_key": self.source_key,
                "source_type": SourceType.to_source_type(self.source_type).name,
            }
        )
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return SourceProvidedBookMetadata(
            **BaseBookMetadata.load_dict(entry)
        )


class ScraperProvidedBookMetadata(BaseBookMetadata):
    def __init__(self, scraper_key, **kwargs):
        super().__init__(**kwargs)
        self.scraper_key = scraper_key

    def as_dict(self):
        ret = super().as_dict()
        ret.update({"scraper_key": self.scraper_key})
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return ScraperProvidedBookMetadata(
            **BaseBookMetadata.load_dict(entry)
        )


class BaseSeriesMetadata(BaseMetadata):
    identifiers: typing.Dict[str, str]
    title: str
    description: str
    tags: typing.List[Tag]

    poster: MetadataImage
    banner: MetadataImage

    def set_metadata(self, **kwargs):
        self._set_dict(kwargs, "identifiers")
        self._set_string(kwargs, "title")
        self._set_string(kwargs, "name", "title")
        self._set_string(kwargs, "description")
        self._set_tags(kwargs, "tags")

        self._set_image(kwargs, "poster")
        self._set_image(kwargs, "banner")

    def as_dict(self):
        if self.poster:
            poster = self.poster.as_dict()
        else:
            poster = None

        if self.banner:
            banner = self.banner.as_dict()
        else:
            banner = None

        tags = {}
        if self.tags:
            tags = {
                tag.key: tag.value
                for tag in self.tags
            }

        return {
            "identifiers": self.identifiers,
            "title": self.title,
            "description": self.description,
            "tags": tags,
            "poster": poster,
            "banner": banner,
        }

    @staticmethod
    def load_dict(entry: typing.Dict[str, typing.Any]):
        loaded = entry.copy()
        if "poster" in entry:
            loaded["poster"] = MetadataImage.from_dict(
                entry["poster"]
            )
        if "banner" in entry:
            loaded["banner"] = MetadataImage.from_dict(
                entry["banner"]
            )
        return loaded

    def __str__(self):
        return self.title


class UserProvidedSeriesMetadata(BaseSeriesMetadata):
    def __init__(self, series_id: str, **kwargs):
        super().__init__(**kwargs)
        self.series_id = series_id

    @property
    def _default_list(self):
        return None

    @property
    def _default_dict(self):
        return None

    def as_dict(self):
        ret = super().as_dict()
        return ret

    @staticmethod
    def from_dict(series_id: str, entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return UserProvidedSeriesMetadata(
            series_id,
            **BaseSeriesMetadata.load_dict(entry)
        )


class SourceProvidedSeriesMetadata(BaseSeriesMetadata):
    def __init__(self, source_key, **kwargs):
        super().__init__(**kwargs)

        self.source_key = source_key

    def as_dict(self):
        ret = super().as_dict()
        ret.update(
            {
                "source_key": self.source_key
            }
        )
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return SourceProvidedSeriesMetadata(
            **BaseSeriesMetadata.load_dict(entry)
        )


class ScraperProvidedSeriesMetadata(BaseSeriesMetadata):
    def __init__(self, scraper_key, **kwargs):
        super().__init__(**kwargs)
        self.scraper_key = scraper_key

    def as_dict(self):
        ret = super().as_dict()
        ret.update({"scraper_key": self.scraper_key})
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return ScraperProvidedSeriesMetadata(
            **BaseSeriesMetadata.load_dict(entry)
        )


class SeriesMetadata:
    identifiers: typing.Dict[str, str]
    title: str
    description: str
    tags: typing.List[Tag]

    poster: MetadataImage
    banner: MetadataImage

    def __init__(
        self,
        series_id: str,
        user_metadata: typing.Optional[UserProvidedSeriesMetadata] = None,
        source_metadata: typing.Optional[
            typing.Dict[str, SourceProvidedSeriesMetadata]
        ] = None,
        scraper_metadata: typing.Optional[
            typing.Dict[str, ScraperProvidedSeriesMetadata]
        ] = None
    ):
        self.series_id = series_id
        if not self.series_id:
            raise Exception("SeriesMetadata missing series_id")

        if user_metadata is None:
            user_metadata = UserProvidedSeriesMetadata(series_id)

        self.user_metadata: UserProvidedSeriesMetadata = user_metadata

        self.source_metadata: typing.Dict[
            str,
            SourceProvidedSeriesMetadata
        ] = source_metadata or {}

        self.scraper_metadata: typing.Dict[
            str,
            ScraperProvidedSeriesMetadata
        ] = scraper_metadata or {}

    def __str__(self):
        return self.title

    def __repr__(self):
        return "{}({})".format(
            type(self).__name__,
            ", ".join(
                f"{param}={repr(value)}" for param, value in self.as_dict().items()
            ),
        )

    def __getattr__(self, name):
        value = getattr(self.user_metadata, name)
        if value or value == 0:
            return value

        values = None
        for metadata in self.scraper_metadata.values():
            value = getattr(metadata, name)
            if value or value == 0:
                if isinstance(value, list):
                    values = value.copy()
                else:
                    return value

        for metadata in self.source_metadata.values():
            value = getattr(metadata, name)
            if value or value == 0:
                if values:
                    return values + value
                return value

        return value

    def as_dict(self):
        ret = {
            "series_id": self.series_id,
            "source_metadata": {
                key: metadata.as_dict()
                for key, metadata in self.source_metadata.items()
            },
            "scraper_metadata": {
                key: metadata.as_dict()
                for key, metadata in self.scraper_metadata.items()
            },
        }
        if self.user_metadata:
            ret["user_metadata"] = self.user_metadata.as_dict()
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        series_id = entry["series_id"]
        source_metadata = {
            key: SourceProvidedSeriesMetadata.from_dict(metadata)
            for key, metadata in entry.get("source_metadata", {}).items()
        }
        scraper_metadata = {
            key: ScraperProvidedSeriesMetadata.from_dict(metadata)
            for key, metadata in entry.get("scraper_metadata", {}).items()
        }
        if "user_metadata" in entry:
            user_metadata = UserProvidedSeriesMetadata.from_dict(
                series_id,
                entry["user_metadata"]
            )
        else:
            user_metadata = None

        return SeriesMetadata(
            series_id=series_id,
            source_metadata=source_metadata,
            scraper_metadata=scraper_metadata,
            user_metadata=user_metadata
        )


class SeriesEntryMetadata(BaseMetadata):
    series_id: str
    volume: typing.List[str]
    issue: typing.List[str]
    primary: bool

    def set_metadata(self, **kwargs):
        self._set_string(kwargs, "series_id")
        self._set_list(kwargs, "volume")
        self._set_list(kwargs, "issue")
        self._set_bool(kwargs, "primary")

    def __str__(self):
        return "Volume: {}, Issue: {} (Series: {}, Primary: {})".format(
            self.volume,
            self.issue,
            self.series_id,
            self.primary
        )

    def as_dict(self):
        """Returns the series entry as a dictonary"""

        return {
            "series_id": self.series_id,
            "volume": self.volume,
            "issue": self.issue,
            "primary": self.primary
        }

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return SeriesEntryMetadata(
            **entry
        )


class ReadingListEntryMetadata(BaseMetadata):
    series_id: str
    index: int

    def set_metadata(self, **kwargs):
        self._set_string(kwargs, "series_id")
        self._set_number(kwargs, "index")

    def __str__(self):
        return "index: {} (Series: {})".format(
            self.index,
            self.series_id
        )

    def as_dict(self):
        """Returns the read list entry as a dictonary"""

        return {
            "series_id": self.series_id,
            "volume": self.volume,
            "issue": self.issue,
            "primary": self.primary
        }

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return ReadingListEntryMetadata(
            **entry
        )


class BasePersonMetadata(BaseMetadata):
    identifiers: typing.Dict[str, str]
    first_name: str
    middle_names: str
    last_name: str
    bio: str
    tags: typing.List[Tag]
    poster: MetadataImage

    def set_metadata(self, **kwargs):
        full_name = kwargs.get("name")
        if full_name:
            parsed_name = nameparser.HumanName(full_name)
            if parsed_name.first:
                kwargs.setdefault("first_name", parsed_name.first)

            if parsed_name.middle:
                kwargs.setdefault("middle_names", parsed_name.middle)

            if parsed_name.last:
                kwargs.setdefault("last_name", parsed_name.last)

        self._set_dict(kwargs, "identifiers")
        self._set_string(kwargs, "first_name")
        self._set_string(kwargs, "middle_names")
        self._set_string(kwargs, "last_name")
        self._set_string(kwargs, "bio")
        self._set_tags(kwargs, "tags")

        self._set_image(kwargs, "poster")

    @property
    def name(self):
        value = ""
        if self.first_name:
            value = self.first_name
        if self.middle_names:
            if value:
                value += " "
            value += self.middle_names
        if self.last_name:
            if value:
                value += " "
            value += self.last_name
        return value or None

    def as_dict(self):
        if self.poster:
            poster = self.poster.as_dict()
        else:
            poster = None

        tags = {}
        if self.tags:
            tags = {
                tag.key: tag.value
                for tag in self.tags
            }

        return {
            "identifiers": self.identifiers,
            "first_name": self.first_name,
            "middle_names": self.middle_names,
            "last_name": self.last_name,
            "bio": self.bio,
            "tags": tags,
            "poster": poster
        }

    @staticmethod
    def load_dict(entry: typing.Dict[str, typing.Any]):
        loaded = entry.copy()
        if "poster" in entry:
            loaded["poster"] = MetadataImage.from_dict(
                entry["poster"]
            )
        return loaded

    def __str__(self):
        return self.name


class UserProvidedPersonMetadata(BasePersonMetadata):
    def __init__(self, person_id: str, **kwargs):
        super().__init__(**kwargs)
        self.person_id = person_id

    @property
    def _default_list(self):
        return None

    @property
    def _default_dict(self):
        return None

    def as_dict(self):
        ret = super().as_dict()
        return ret

    @staticmethod
    def from_dict(person_id: str, entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return UserProvidedPersonMetadata(
            person_id,
            **BasePersonMetadata.load_dict(entry)
        )


class SourceProvidedPersonMetadata(BasePersonMetadata):
    def __init__(self, source_key, **kwargs):
        super().__init__(**kwargs)

        self.source_key = source_key

    def as_dict(self):
        ret = super().as_dict()
        ret.update(
            {
                "source_key": self.source_key
            }
        )
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return SourceProvidedPersonMetadata(
            **BasePersonMetadata.load_dict(entry)
        )


class ScraperProvidedPersonMetadata(BasePersonMetadata):
    def __init__(self, scraper_key, **kwargs):
        super().__init__(**kwargs)
        self.scraper_key = scraper_key

    def as_dict(self):
        ret = super().as_dict()
        ret.update({"scraper_key": self.scraper_key})
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        return ScraperProvidedPersonMetadata(
            **BasePersonMetadata.load_dict(entry)
        )


class PersonMetadata:
    identifiers: typing.Dict[str, str]
    first_name: str
    middle_names: str
    last_name: str
    bio: str
    tags: typing.List[Tag]
    poster: MetadataImage

    def __init__(
        self,
        person_id: str,
        user_metadata: typing.Optional[UserProvidedPersonMetadata] = None,
        source_metadata: typing.Optional[
            typing.Dict[str, SourceProvidedPersonMetadata]
        ] = None,
        scraper_metadata: typing.Optional[
            typing.Dict[str, ScraperProvidedPersonMetadata]
        ] = None
    ):
        self.person_id = person_id
        if not self.person_id:
            raise Exception("PersonMetadata missing person_id")

        if user_metadata is None:
            user_metadata = UserProvidedPersonMetadata(person_id)

        self.user_metadata: UserProvidedPersonMetadata = user_metadata

        self.source_metadata: typing.Dict[
            str,
            SourceProvidedPersonMetadata
        ] = source_metadata or {}

        self.scraper_metadata: typing.Dict[
            str,
            ScraperProvidedPersonMetadata
        ] = scraper_metadata or {}

    def __str__(self):
        return self.title

    def __repr__(self):
        return "{}({})".format(
            type(self).__name__,
            ", ".join(
                f"{param}={repr(value)}" for param, value in self.as_dict().items()
            ),
        )

    def __getattr__(self, name):
        value = getattr(self.user_metadata, name)
        if value or value == 0:
            return value

        values = None
        for metadata in self.scraper_metadata.values():
            value = getattr(metadata, name)
            if value or value == 0:
                if isinstance(value, list):
                    values = value.copy()
                else:
                    return value

        for metadata in self.source_metadata.values():
            value = getattr(metadata, name)
            if value or value == 0:
                if values:
                    return values + value
                return value

        return value

    def as_dict(self):
        ret = {
            "person_id": self.person_id,
            "source_metadata": {
                key: metadata.as_dict()
                for key, metadata in self.source_metadata.items()
            },
            "scraper_metadata": {
                key: metadata.as_dict()
                for key, metadata in self.scraper_metadata.items()
            },
        }
        if self.user_metadata:
            ret["user_metadata"] = self.user_metadata.as_dict()
        return ret

    @staticmethod
    def from_dict(entry: typing.Dict[str, typing.Any]):
        if entry is None:
            return None

        person_id = entry["person_id"]
        source_metadata = {
            key: SourceProvidedPersonMetadata.from_dict(metadata)
            for key, metadata in entry.get("source_metadata", {}).items()
        }
        scraper_metadata = {
            key: ScraperProvidedPersonMetadata.from_dict(metadata)
            for key, metadata in entry.get("scraper_metadata", {}).items()
        }
        if "user_metadata" in entry:
            user_metadata = UserProvidedPersonMetadata.from_dict(
                person_id,
                entry["user_metadata"]
            )
        else:
            user_metadata = None

        return PersonMetadata(
            person_id=person_id,
            source_metadata=source_metadata,
            scraper_metadata=scraper_metadata,
            user_metadata=user_metadata
        )


class FileAccessor(ABC):
    def open(self):
        pass

    def close(self):
        pass

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    @abstractmethod
    def get_local_path(self):
        pass

    @abstractmethod
    def get_filename(self):
        pass

    @abstractmethod
    def get_source_size(self):
        pass

    @abstractmethod
    def get_source_modified_date(self):
        pass

    @abstractmethod
    async def get_stream(self):
        pass

    async def get_sub_accessor_names(self):
        return []

    async def get_sub_accessor(self, name: str):
        return None


class BookSource:
    """Represents a book's source, and everything needed to work with it."""

    @property
    def source_key(self) -> str:
        """Returns the source key for this source."""
        return self.get_metadata().source_key

    @property
    def source_type(self):
        """Returns the type of source."""
        return self.get_metadata().source_type

    @abstractmethod
    def get_metadata(self) -> SourceProvidedBookMetadata:
        """Returns metadata for this book."""

    @abstractmethod
    async def get_accessor(self) -> FileAccessor:
        """Returns a file accessor for this book source."""


class Book:
    """Represents a book, and everything needed to work with it."""

    @property
    def book_id(self) -> str:
        """Returns the book ID for this source."""
        return self.get_metadata().book_id

    @abstractmethod
    def get_metadata(self) -> BookMetadata:
        """Returns metadata for this book."""

    @abstractmethod
    async def get_sources(self) -> typing.List[BookSource]:
        """Returns a BookSource for each source of this book."""
