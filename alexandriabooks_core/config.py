import logging
import typing
import os
import os.path
import contextvars
import collections
import re
from threading import Lock
import yaml

__CONFIG__: typing.Dict[str, str] = {}
__PROFILE_CONFIG__: typing.Dict[str, typing.Dict[str, str]] = {}

__OS_ENV_PREFIX__: str = "ALEXANDRIABOOKS_"
__OS_ENV_PROFILE_PREFIX__: str = "ALEXANDRIABOOKSPROFILE_"
__CFG_OS_ENV__: str = "ALEXANDRIABOOKSCONFIG"
__CFG_DEFAULT__: str = "alexandria.yml"
__CFG_PROFILE_PREFIX__: str = "alexandria_profile."
__CFG_PROFILE_SUFFIX__: str = ".yml"


__CONFIG_LOCK__ = Lock()
__PROFILE_NAME__: contextvars.ContextVar = contextvars.ContextVar("profile")

logger = logging.getLogger(__name__)


def get_config_file() -> str:
    return os.environ.get(__CFG_OS_ENV__, __CFG_DEFAULT__)


def __walk_config__(dest_config, parent, src_config):
    if src_config:
        for key, value in src_config.items():
            normalized_key = parent + key.upper().replace(".", "_").strip("_")
            if isinstance(value, dict):
                __walk_config__(dest_config, normalized_key + "_", value)
            else:
                dest_config[normalized_key] = str(value)


def __load_config__():
    with __CONFIG_LOCK__:
        if not __CONFIG__:
            logger.info("Loading configuration...")

            config_file = get_config_file()
            if os.path.isfile(config_file):
                logger.info(f"Loading configuration from {config_file}")
                with open(config_file, "r") as stream:
                    loaded_config = yaml.safe_load(stream)

                __walk_config__(__CONFIG__, "", loaded_config)

            for key, value in os.environ.items():
                if key.startswith(__OS_ENV_PREFIX__):
                    normalized_key = key[len(__OS_ENV_PREFIX__):].upper().strip("_")
                    __CONFIG__[normalized_key] = value
            __CONFIG__["CONFIG_LOADED"] = "1"

        profile_name = __PROFILE_NAME__.get(None)
        if not profile_name:
            return __CONFIG__

        profile_config = __PROFILE_CONFIG__.get(profile_name)
        if profile_config is not None:
            return profile_config

        logger.info(f"Loading profile {profile_name}...")

        profile_file = f"{__CFG_PROFILE_PREFIX__}{profile_name}{__CFG_PROFILE_SUFFIX__}"
        profile_config = {}

        if os.path.isfile(profile_file):
            logger.info(f"Loading profile {profile_name} from {profile_file}...")
            with open(profile_file, "r") as stream:
                loaded_config = yaml.safe_load(stream)

            __walk_config__(profile_config, "", loaded_config)

        env_profile_prefix = f"{__OS_ENV_PROFILE_PREFIX__}{profile_name.upper()}_"
        for key, value in os.environ.items():
            if key.startswith(env_profile_prefix):
                normalized_key = key[len(env_profile_prefix):].upper().strip("_")
                profile_config[normalized_key] = value

            profile_config = collections.ChainMap(profile_config, __CONFIG__)
        __PROFILE_CONFIG__[profile_name] = profile_config
        return profile_config


def reload():
    __CONFIG__.clear()
    __PROFILE_CONFIG__.clear()


def set_profile(profile_name: typing.Optional[str]) -> typing.Optional[str]:
    if profile_name is not None:
        if not profile_name:
            # Empty name is no profile
            profile_name = None
        else:
            # Normalize profile name, max 16 characters, only latin alpha and numbers.
            profile_name = re.sub("[^0-9a-zA-Z]+", "", profile_name)[:16].lower()
    __PROFILE_NAME__.set(profile_name)
    return profile_name


def get(key: str, default: typing.Optional[str] = None) -> typing.Optional[str]:
    normalized_key = key.upper().replace(".", "_").strip("_")
    return __load_config__().get(normalized_key, default)


def get_bool(key: str, default: bool = False) -> bool:
    result = get(key)
    if result is None:
        return default
    result = str(result).lower()
    if not result:
        return default
    return result[0] in ["1", "y", "t"]


def set_value(key: str, value: typing.Any):
    normalized_key = key.upper().replace(".", "_").strip("_")
    __load_config__()[normalized_key] = str(value)


def get_prefix(prefix: str) -> typing.Dict[str, str]:
    normalized_prefix = prefix.upper().replace(".", "_").strip("_")
    values = {}
    for key, value in __load_config__().items():
        if key.startswith(normalized_prefix):
            values[key[len(normalized_prefix):].strip("_")] = value
    return values
