import os
import os.path
import datetime
import typing
from abc import ABC, abstractmethod

from .model import FileAccessor


class LocalBookAccessor(FileAccessor):
    def __init__(self, path: str):
        self.path = path
        self.__file_stat = None

    def _get_file_stat(self):
        if self.__file_stat is None:
            self.__file_stat = os.stat(self.path)
        return self.__file_stat

    def get_local_path(self):
        return self.path

    def get_filename(self):
        return os.path.basename(self.path)

    def get_source_size(self):
        return self._get_file_stat().st_size

    def get_source_modified_date(self):
        return datetime.datetime.fromtimestamp(
            self._get_file_stat().st_mtime
        )

    async def get_stream(self):
        return open(self.path, "rb")


class ArchiveBookAccessor(FileAccessor, ABC):
    def __init__(self, accessor: FileAccessor):
        self.accessor = accessor

    async def get_archive_comment(self):
        return None

    @abstractmethod
    async def get_file_listing(self) -> typing.List[str]:
        pass

    @abstractmethod
    async def get_file_stream(self, path):
        pass

    def get_file_size(self, path):
        return None

    def get_local_path(self):
        return self.accessor.get_local_path()

    def get_filename(self):
        return self.accessor.get_filename()

    def get_source_size(self):
        return self.accessor.get_source_size()

    def get_source_modified_date(self):
        return self.accessor.get_source_modified_date()

    async def get_stream(self):
        return await self.accessor.get_stream()

    async def get_file_streams(self):
        return [
            (filename, await self.get_file_stream(filename))
            for filename in await self.get_file_listing()
        ]

    async def get_tree(self, flattened=False):
        dir_dict = {}
        if flattened:
            files = []
            dir_dict["FILES"] = files
            for path in await self.get_file_listing():
                if path.endswith("/") or path.endswith("\\"):
                    continue
                files.append(path)
        else:
            for path in [
                os.path.normpath(item).split(os.sep)
                for item in await self.get_file_listing()
                if not (item.endswith("/") or item.endswith("\\"))
            ]:
                current = dir_dict
                for seg in path[:-1]:
                    if "DIRS" not in current:
                        current["DIRS"] = {}
                    current = current["DIRS"]
                    if seg not in current:
                        current[seg] = {}
                    current = current[seg]
                if "FILES" not in current:
                    current["FILES"] = []
                current["FILES"].append(path[-1])
        return dir_dict
