import typing
import re
import datetime
import asyncio


__DATE_PARSER__ = re.compile(
    r"^([12][0-9][0-9][0-9])[^0-9]?([01][0-9])[^0-9]?([0123][0-9])"
)


async def copy_streams(
    in_stream: typing.Optional[typing.IO[bytes]],
    out_stream: typing.IO[bytes],
    buffer_size: int = 4096
):
    def do_copy():
        size = 0
        while True:
            buffer = in_stream.read(buffer_size)
            amount = len(buffer)
            if not amount:
                return size
            size += amount
            out_stream.write(buffer)

    if in_stream:
        await asyncio.get_running_loop().run_in_executor(None, do_copy)


def ensure_list(
    value: typing.Any,
    default: typing.List[typing.Any],
    deduplicate: bool = True
):
    if not value and value != 0:
        return default

    if isinstance(value, tuple):
        value = list(value)
    elif not isinstance(value, list):
        value = [value]

    if deduplicate:
        dedup_value: typing.List[typing.Any] = []
        for item in value:
            if value or value == 0 and item not in dedup_value:
                dedup_value.append(item)
        return dedup_value
    else:
        return [
            entry
            for entry in value
            if entry or entry == 0
        ]


def ensure_dict(value: typing.Any, default: typing.Dict[str, typing.Any]):
    if not value and value != 0:
        return default
    return value


def ensure_date(
    value: typing.Any,
    default: typing.Optional[datetime.date]
) -> typing.Optional[datetime.date]:
    if not value and value != 0 and value is not False:
        return default
    elif isinstance(value, datetime.date):
        return value
    elif isinstance(value, datetime.datetime):
        return value.date()
    elif isinstance(value, int):
        return datetime.datetime.fromordinal(value).date()
    elif isinstance(value, float):
        return datetime.datetime.fromtimestamp(value).date()
    elif isinstance(value, str):
        matches = __DATE_PARSER__.match(value)
        if matches:
            groups = matches.groups()
            if len(groups) >= 3:
                try:
                    return datetime.date(
                        year=int(groups[0]),
                        month=int(groups[1]),
                        day=int(groups[2])
                    )
                except ValueError:
                    pass
    return default


def ensure_number(value: typing.Any, default: int):
    if not value and value != 0 and value is not False:
        return default
    if isinstance(value, int):
        return value
    try:
        return int(value)
    except ValueError:
        return default


def ensure_string(value: typing.Any, default: str):
    if not value and value != 0 and value is not False:
        return default
    if isinstance(value, str):
        return value
    return str(value)


def ensure_bool(value: typing.Any, default: bool) -> bool:
    if not value and value != 0 and value is not False:
        return default
    if isinstance(value, str):
        if value:
            return value[0].lower() in ['t', '1', 'y']
        return False
    return bool(value)


# Shameless borrowed from django.
__VALID_FILENAME_RE__ = re.compile(r"(?u)[^-\w.]")


def get_valid_filename(value):
    value = str(value).strip().replace(" ", "_")
    return __VALID_FILENAME_RE__.sub("", value)


__VOLUME_ISSUE_RE__ = re.compile(r"^((?P<series_name>.*?)[:\s,–-]*([Oo][Mm][Nn][Ii][Bb][Uu][Ss]|([Oo][Mm][Nn][Ii][Bb][Uu][Ss]\s+)?[Vv][Oo][Ll]\.?|([Oo][Mm][Nn][Ii][Bb][Uu][Ss]\s+)?[Vv][Oo][Ll][Uu][Mm][Ee])\s*#?(?P<volume_number>[0-9]+([\s–-]*?[0-9]+)?))?((.*?)[\s,–-]*(#|[Ii][Ss][Ss][Uu][Ee]\s*#?)\s*(?P<issue_number>[0-9]+([\s–-]*?[0-9]+)?))?([\s:–-]*(?P<issue_name>.*))?$")  # noqa: E501
__ISSUE_VOLUME_RE__ = re.compile(r"^((?P<series_name>.*?)[:\s,–-]*(#|[Ii][Ss][Ss][Uu][Ee]|[Ii][Ss][Ss][Uu][Ee]\s*#?)\s*(?P<issue_number>[0-9]+([\s–-]*?[0-9]+)?))?((.*?)[\s,–-]*([Oo][Mm][Nn][Ii][Bb][Uu][Ss]|([Oo][Mm][Nn][Ii][Bb][Uu][Ss]\s+)?[Vv][Oo][Ll]\.?|([Oo][Mm][Nn][Ii][Bb][Uu][Ss]\s+)?[Vv][Oo][Ll][Uu.]?[Mm]?[Ee])?\s*#?(?P<volume_number>[0-9]+([\s–-]*?[0-9]+)?))?([\s:,–-]*(?P<issue_name>.*))?$")  # noqa: E501


def parse_book_title(
    text: str
) -> typing.Tuple[
    typing.Optional[str],
    typing.Optional[str],
    typing.Optional[str],
    typing.Optional[str]
]:
    """
    Parses a book title into volume name, issue name, volume number,
    issue number, if possible.
    """
    vi_series_name = ""
    vi_volume = ""
    vi_issue = ""
    vi_title = ""

    match = __VOLUME_ISSUE_RE__.fullmatch(text)
    if match:
        groups = match.groupdict()
        vi_series_name = groups.get("series_name") or vi_series_name
        vi_title = groups.get("issue_name") or vi_title
        vi_volume = groups.get("volume_number") or vi_volume
        vi_issue = groups.get("issue_number") or vi_issue

    vi_score = 0
    if vi_series_name:
        vi_score += 1
    if vi_volume:
        vi_score += 1
    if vi_issue:
        vi_score += 1
    if vi_title:
        vi_score += 1
    if "volume" in vi_title.lower() or "issue" in vi_title.lower():
        vi_score -= 1

    iv_series_name = ""
    iv_volume = ""
    iv_issue = ""
    iv_title = ""

    match = __ISSUE_VOLUME_RE__.fullmatch(text)
    if match:
        groups = match.groupdict()
        iv_series_name = groups.get("series_name") or iv_series_name
        iv_title = groups.get("issue_name") or iv_title
        iv_volume = groups.get("volume_number") or iv_volume
        iv_issue = groups.get("issue_number") or iv_issue

    iv_score = 0
    if iv_series_name:
        iv_score += 1
    if iv_volume:
        iv_score += 1
    if iv_issue:
        iv_score += 1
    if iv_title:
        iv_score += 1
    if "volume" in iv_title.lower() or "issue" in iv_title.lower():
        iv_score -= 1

    if iv_score > vi_score:
        return iv_series_name, iv_title, iv_volume, iv_issue

    return vi_series_name, vi_title, vi_volume, vi_issue
