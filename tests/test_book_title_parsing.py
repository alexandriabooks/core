from alexandriabooks_core.utils import parse_book_title

def test_vol():
    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("The Evil Within Vol. 1")

    assert parsed_volume_name == "The Evil Within"
    assert parsed_issue_name == ""
    assert parsed_volume_number == "1"
    assert parsed_issue_number == ""

    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("Army of Darkness Omnibus Vol. 1")

    assert parsed_volume_name == "Army of Darkness"
    assert parsed_issue_name == ""
    assert parsed_volume_number == "1"
    assert parsed_issue_number == ""
    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("Elephantmen Vol. 00: Armed Forces")

    assert parsed_volume_name == "Elephantmen"
    assert parsed_issue_name == "Armed Forces"
    assert parsed_volume_number == "00"
    assert parsed_issue_number == ""

def test_volume():
    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("xkcd: volume 0")

    assert parsed_volume_name == "xkcd"
    assert parsed_issue_name == ""
    assert parsed_volume_number == "0"
    assert parsed_issue_number == ""
    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("Doctor Who - Short Trips: Volume 3")

    assert parsed_volume_name == "Doctor Who - Short Trips"
    assert parsed_issue_name == ""
    assert parsed_volume_number == "3"
    assert parsed_issue_number == ""

def test_issue():
    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("Free Issue Alice Cooper #1")

    assert parsed_volume_name == "Free Issue Alice Cooper"
    assert parsed_issue_name == ""
    assert parsed_volume_number == ""
    assert parsed_issue_number == "1"

def test_volume_issue():
    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("Red Sonja Volume 2 #1")

    assert parsed_volume_name == "Red Sonja"
    assert parsed_issue_name == ""
    assert parsed_volume_number == "2"
    assert parsed_issue_number == "1"

def test_issue_volume():
    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("The Walking Dead, Issue 2, Volume: 3")

    assert parsed_volume_name == "The Walking Dead"
    assert parsed_issue_name == ""
    assert parsed_volume_number == "3"
    assert parsed_issue_number == "2"

    (
        parsed_volume_name,
        parsed_issue_name,
        parsed_volume_number,
        parsed_issue_number
    ) = parse_book_title("Free Issue Legenderry: A Steampunk Adventure #3, Volume: 4")

    assert parsed_volume_name == "Free Issue Legenderry: A Steampunk Adventure"
    assert parsed_issue_name == ""
    assert parsed_volume_number == "4"
    assert parsed_issue_number == "3"